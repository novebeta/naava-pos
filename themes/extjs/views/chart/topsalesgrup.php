<script type="text/javascript">
    window.onload = function () {
        var data = '<?php echo $data; ?>';
        var json = JSON.parse(data);
        var chart = new CanvasJS.Chart("chartContainer",
            {
                backgroundColor: null,
                animationEnabled: true,
                title: {
                    text: 'Top <?php echo "$limit $grup_name $chart_title"?>'
                },
                legend: {
                    verticalAlign: "bottom",
                    horizontalAlign: "center"
                },
                axisX:{
                    labelFontSize: 14,
                    interval: 1
                },
                data: [
                    {
                        type: "column",
                        dataPoints: json
                    }
                ]
            });
        chart.render();
    }
</script>
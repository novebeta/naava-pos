jun.Cardstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Cardstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'CardStoreId',
            url: 'Card',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'card_id'},
                {name: 'card_name'},
                {name: 'persen_fee'},
                {name: 'up'},
            ]
        }, cfg));
    }
});
jun.rztCard = new jun.Cardstore();
jun.rztCardLib = new jun.Cardstore();
jun.rztCardCmp = new jun.Cardstore();
//jun.rztCard.load();

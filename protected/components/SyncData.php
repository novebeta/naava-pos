<?php
class SyncData
{
    private $result;
    private $username;
    private $password;
    private $status_error;
    private $skip_primary;
    public function sync()
    {
        $this->skip_primary = array(
            'Payment',
            'StockMoves',
            'BankTrans',
            'TenderDetails',
            'TransferItemDetails',
            'GlTrans',
            'Comments',
            'BeautyServices',
            'PrintzDetails',
            'Refs',
            'Jual',
            'GrupAttr'
        );
        Yii::app()->db->createCommand("
        ALTER TABLE nscc_security_roles AUTO_INCREMENT = 1;
        ALTER TABLE nscc_gol AUTO_INCREMENT = 1;
        ALTER TABLE nscc_trans_tipe AUTO_INCREMENT = 1;
        ALTER TABLE nscc_status_cust AUTO_INCREMENT = 1;
        ALTER TABLE nscc_supplier AUTO_INCREMENT = 1;
        ALTER TABLE nscc_kategori AUTO_INCREMENT = 1;
        ALTER TABLE nscc_negara AUTO_INCREMENT = 1;
        ALTER TABLE nscc_provinsi AUTO_INCREMENT = 1;
        ALTER TABLE nscc_kota AUTO_INCREMENT = 1;
        ALTER TABLE nscc_kecamatan AUTO_INCREMENT = 1;
        ALTER TABLE nscc_store AUTO_INCREMENT = 1;
        ALTER TABLE nscc_card AUTO_INCREMENT = 1;
        ALTER TABLE nscc_barang AUTO_INCREMENT = 1;
        ALTER TABLE nscc_grup AUTO_INCREMENT = 1;")
            ->execute(array());
        $this->username = yiiparam('Username');
        $this->password = yiiparam('Password');
        app()->db->autoCommit = false;
        if (is_connected(NARSDOMAIN, NARSPORT)) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                self::upload_cust_nars();
                self::upload_trans_nars();
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollback();
//                $msg['NARS_ERROR'] = $ex->getMessage();
                self::echo_log('NARS_ERROR', 'NARS', $ex->getMessage());
            }
        } else {
//            $msg['NARS_ERROR'] = 'No connection';
            self::echo_log('NARS_ERROR', 'NARS', 'No connection');
        }
        if (is_connected('imap.gmail.com', 993) && is_connected('smtp.gmail.com', 587)) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                Yii::app()->db->createCommand("SET FOREIGN_KEY_CHECKS = 0")
                    ->execute();
                self::download_global();
                Yii::app()->db->createCommand("SET FOREIGN_KEY_CHECKS = 1")
                    ->execute();
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollback();
//                $msg['GLOBAL_DOWN_ERROR'] = $ex->getMessage();
                self::echo_log('GLOBAL_DOWN_ERROR', 'GLOBAL', $ex->getMessage());
            }
            $transaction = Yii::app()->db->beginTransaction();
            try {
                self::upload_global();
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollback();
//                $msg['GLOBAL_UP_ERROR'] = $ex->getMessage();
                self::echo_log('GLOBAL_UP_ERROR', 'GLOBAL', $ex->getMessage());
            }
            $transaction = Yii::app()->db->beginTransaction();
            try {
                Yii::app()->db->createCommand("SET FOREIGN_KEY_CHECKS = 0")
                    ->execute();
                self::download_master();
                Yii::app()->db->createCommand("SET FOREIGN_KEY_CHECKS = 1")
                    ->execute();
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollback();
//                $msg['MASTER_DOWN_ERROR'] = $ex->getMessage();
                self::echo_log('MASTER_DOWN_ERROR', 'MASTER', $ex->getMessage());
            }
            if (BROADCAST) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    self::broadcast_master();
                    $transaction->commit();
                } catch (Exception $ex) {
                    $transaction->rollback();
//                    $msg['BROADCAST_UP_ERROR'] = $ex->getMessage();
                    self::echo_log('BROADCAST_UP_ERROR', 'MASTER', $ex->getMessage());
                }
            }
            if (TOSTOREHO) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    self::upload_master_all();
                    $transaction->commit();
                } catch (Exception $ex) {
                    $transaction->rollback();
//                    $msg['MASTERHO_UP_ERROR'] = $ex->getMessage();
                    self::echo_log('MASTERHO_UP_ERROR', 'MASTER', $ex->getMessage());
                }
            }
            $transaction = Yii::app()->db->beginTransaction();
            try {
                Yii::app()->db->createCommand("SET FOREIGN_KEY_CHECKS = 0")
                    ->execute();
                self::download_transaksi();
                Yii::app()->db->createCommand("SET FOREIGN_KEY_CHECKS = 1")
                    ->execute();
                $transaction->commit();
            } catch (Exception $ex) {
                $transaction->rollback();
//                $msg['TRANS_DOWN_ERROR'] = $ex->getMessage();
                self::echo_log('TRANS_DOWN_ERROR', 'TRANSACTION', $ex->getMessage());
            }
            if (TOSTOREHO) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    self::upload_transaksi();
                    $transaction->commit();
                } catch (Exception $ex) {
                    $transaction->rollback();
//                    $msg['TRANS_UP_ERROR'] = $ex->getMessage();
                    self::echo_log('TRANS_UP_ERROR', 'TRANSACTION', $ex->getMessage());
                }
            }
            if (!HEADOFFICE) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    self::remainder();
                    $transaction->commit();
                } catch (Exception $ex) {
                    $transaction->rollback();
//                    $msg['send_remainder'] = $ex->getMessage();
                    self::echo_log('SEND_REMAINDER', 'REMAINDER', $ex->getMessage());
                }
            }
//            $msg['LOG'] = $this->result;
        } else {
//            $msg['LOG'] = 'No connection';
            self::echo_log('EMAIL', 'IMAP', 'No connection');
        }
        app()->db->autoCommit = true;
        Yii::app()->db->createCommand("DELETE FROM nscc_sync WHERE DATEDIFF(NOW(),tdate) > :days")
            ->execute(array(":days" => DAYSBEFOREDELSYNC));
//        return $msg;
    }
    private function upload_cust_nars()
    {
        $cust = Customers::get_backup();
        if (count($cust) == 0) {
//            $this->result['upload_cust_nars'] = 'nothing to upload';
            self::echo_log('NARS', 'UPLOAD_CUST_NARS', 'No connection');
            return;
        }
        $url = NARSURL;
        $data = array(
            'username' => SysPrefs::get_val('username_nars'),
            'password' => SysPrefs::get_val('password_nars'),
            'cabang' => STOREID,
            'tanggal' => "",
            'jenis' => 'pasien',
            'data' => CJSON::encode($cust)
        );
        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'ignore_errors' => true,
                'content' => http_build_query($data),
            ),
        );
        $context = stream_context_create($options);
        $result = @file_get_contents($url, false, $context);
        if ($result == '1') {
            $r = Yii::app()->db->createCommand("
           UPDATE nscc_customers SET log = 1 WHERE log = 0
           ")->execute();
//            $this->result['upload_cust_nars'] = 'success upload ' . number_format($r) . ' records';
            self::echo_log('NARS', 'UPLOAD_CUST_NARS', 'success upload ' . number_format($r) . ' records');
            return;
        }
//        $this->result['upload_cust_nars'] = 'failed upload records';
        self::echo_log('upload_cust_nars', 'NARS', 'failed upload records');
    }
    private function upload_trans_nars()
    {
        $sales = Salestrans::get_backup();
        if (count($sales) == 0) {
//            $this->result['upload_trans_nars'] = 'nothing to upload';
            self::echo_log('UPLOAD_TRANS_NARS', 'NARS', 'nothing to upload');
            return;
        }
        $url = NARSURL;
        $data = array(
            'username' => SysPrefs::get_val('username_nars'),
            'password' => SysPrefs::get_val('password_nars'),
            'cabang' => STOREID,
            'tanggal' => "",
            'jenis' => 'transaksi',
            'data' => CJSON::encode($sales)
        );
        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data),
            ),
        );
        $context = stream_context_create($options);
        $result = @file_get_contents($url, false, $context);
        if ($result == '1') {
            $r = Yii::app()->db->createCommand("
           UPDATE nscc_salestrans SET log = 1 WHERE log = 0
           ")->execute();
//            $this->result['upload_trans_nars'] = 'success upload ' . number_format($r) . ' records';
            self::echo_log('UPLOAD_TRANS_NARS', 'NARS', 'success upload ' . number_format($r) . ' records');
            return;
        }
//        $this->result['upload_trans_nars'] = 'failed upload records';
        self::echo_log('UPLOAD_TRANS_NARS', 'NARS', 'failed upload records');
    }
    private function upload_global()
    {
        $global = array(
            'SecurityRoles',
            'Gol',
            'TransTipe',
            'Customers',
            'StatusCust',
            'ChartMaster',
            'Supplier',
            'Kategori',
            'Negara',
            'Provinsi',
            'Kota',
            'Kecamatan',
            'Store',
            'Card',
            'Barang',
            'Grup'
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        foreach ($global as $modelName) {
            $upload = array();
            $content = array();
            $tables = CActiveRecord::model($modelName)->findAll($criteria);
            if ($tables != null) {
                $upload[$modelName] = $tables;
                $content[$modelName] = number_format(count($tables)) . ' record';
            }
            if (count($upload) == 0) {
//                $this->result['upload_global'][$modelName] = 'nothing to upload';
                self::echo_log('UPLOAD_GLOBAL', $modelName, 'nothing to upload');
                continue;
            }
            $to = yiiparam('Username');
            $time = date('Y-m-d H:i:s');
            $subject = STOREGLOBAL . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($tables as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_global'][$modelName] = 'success upload ' . number_format(count($tables)) . ' records';
                self::echo_log('UPLOAD_GLOBAL', $modelName,
                    'success upload ' . number_format(count($tables)) . ' records');
                $sync = new Sync;
                $sync->type_ = "G";
                $sync->subject = $subject;
                if (!$sync->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                }
                $log = new UploadLog;
                $log->target = STOREGLOBAL;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_global'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_GLOBAL', $modelName, 'failed upload records');
            }
        }
    }
    private function upload_master_cabang($from = STOREID, $target = STOREIDTARGET)
    {
        $master = array(
            'SysPrefs',
            'Users',
            'Bank',
            'Diskon',
            'Price',
            'Dokter',
            'Beauty',
            'Beli',
            'Jual',
            'GrupAttr'
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        $criteria->addCondition("store = :store");
        $criteria->params = array(':store' => $from);
        foreach ($master as $modelName) {
            $upload = array();
            $content = array();
            $tables = CActiveRecord::model($modelName)->findAll($criteria);
            if ($tables != null) {
                $upload[$modelName] = $tables;
                $content[$modelName] = number_format(count($tables)) . ' record';
            }
            if (count($upload) == 0) {
//                $this->result['upload_master_cabang'][$modelName][$target] = 'nothing to upload';
                self::echo_log('UPLOAD_MASTER_CABANG', $modelName . "-" . $target, 'nothing to upload');
                continue;
            }
            $to = yiiparam('Username');
            $time = date('Y-m-d H:i:s');
            $subject = 'M' . $target . '-' . sha1($from . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => $from,
                    'date' => $time,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0 AND store = :store")->execute(array(':store' => $from));
                foreach ($tables as $model) {
                    $model->up = 1;
//                    $model->store = $from;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_master'][$modelName][$target] = 'success upload ' . number_format(count($tables)) . ' records';
                self::echo_log('UPLOAD_MASTER', $modelName . "-" . $target,
                    'success upload ' . number_format(count($tables)) . ' records');
                $log = new UploadLog;
                $log->target = 'M' . $target;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_master'][$modelName][$target] = 'failed upload records';
                self::echo_log('UPLOAD_MASTER', $modelName . "-" . $target, 'failed upload records');
            }
        }
    }
    private function upload_master_all($from = STOREID, $target = STOREIDTARGET)
    {
        $master = array(
            'SysPrefs',
            'Users',
            'Bank',
            'Barang',
            'Price',
            'Dokter',
            'Beauty',
            'Beli',
            'Jual',
            'GrupAttr'
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        foreach ($master as $modelName) {
            $upload = array();
            $content = array();
            $tables = CActiveRecord::model($modelName)->findAll($criteria);
            if ($tables != null) {
                $upload[$modelName] = $tables;
                $content[$modelName] = number_format(count($tables)) . ' record';
            }
            if (count($upload) == 0) {
//                $this->result['upload_master_all'][$modelName][$target] = 'nothing to upload';
                self::echo_log('UPLOAD_MASTER_ALL', $modelName . "-" . $target, 'nothing to upload');
                continue;
            }
            $to = yiiparam('Username');
            $time = date('Y-m-d H:i:s');
            $subject = 'M' . $target . '-' . sha1($from . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => $from,
                    'date' => $time,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($tables as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_master_all'][$modelName][$target] = 'success upload ' . number_format(count($tables)) . ' records';
                self::echo_log('UPLOAD_MASTER_ALL', $modelName . "-" . $target,
                    'success upload ' . number_format(count($tables)) . ' records');
                $log = new UploadLog;
                $log->target = 'M' . $target;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_master'][$modelName][$target] = 'failed upload records';
                self::echo_log('UPLOAD_MASTER', $modelName . "-" . $target, 'failed upload records');
            }
        }
    }
    private function upload_transaksi()
    {
        $trans = array(
            'Refs',
            'Comments',
            'GlTrans',
            'StockMoves',
            'BankTrans'
        );
        $criteria = new CDbCriteria;
        $criteria->addCondition("up = 0");
        $to = yiiparam('Username');
        foreach ($trans as $modelName) {
            $upload = array();
            $content = array();
            $tables = CActiveRecord::model($modelName)->findAll($criteria);
            if ($tables != null) {
                $upload[$modelName] = $tables;
                $content[$modelName] = number_format(count($tables)) . ' record';
            }
            if (count($upload) == 0) {
//                $this->result['upload_transaksi'][$modelName] = 'nothing to upload';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'nothing to upload');
                continue;
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($tables as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($tables)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($tables)) . ' records');
                $log = new UploadLog;
                $log->target = 'T' . STOREIDTARGET;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// Tender ==============================================================================================================
        $tender = Tender::model()->findAll($criteria);
        $upload = array();
        $content = array();
        if ($tender != null) {
            $upload['Tender'] = $tender;
            $modelName = 'Tender';
            $content['Tender'] = number_format(count($tender)) . ' record';
            $tenderDetails = array();
            $printz = array();
            $printzDetails = array();
            foreach ($tender as $row) {
//                $row = new Tender;
                if (!empty($row->tenderDetails)) {
                    $tenderDetails = array_merge($tenderDetails, $row->tenderDetails);
                }
                if (!empty($row->printzs)) {
                    $printz = array_merge($printz, $row->printzs);
                }
                foreach ($row->printzs as $row_printz) {
                    if (!empty($row_printz->printzDetails)) {
                        $printzDetails = array_merge($printzDetails, $row_printz->printzDetails);
                    }
                }
            }
            if (count($tenderDetails) > 0) {
                $upload['TenderDetails'] = $tenderDetails;
                $content['TenderDetails'] = number_format(count($tenderDetails)) . ' record';
            }
            if (!empty($printz)) {
                $upload['Printz'] = $printz;
                $content['Printz'] = number_format(count($printz)) . ' record';
            }
            if (!empty($printzDetails)) {
                $upload['PrintzDetails'] = $printzDetails;
                $content['PrintzDetails'] = number_format(count($printzDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($tender as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($tender)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($tender)) . ' records');
                $log = new UploadLog;
                $log->target = 'T' . STOREIDTARGET;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// Tender ==============================================================================================================
// TransferItem ========================================================================================================
        $transferItem = TransferItem::model()->findAll($criteria);
//        $tender = new TransferItem;
        $upload = array();
        $content = array();
        if ($transferItem != null) {
            $modelName = 'TransferItem';
            $upload['TransferItem'] = $transferItem;
            $content['TransferItem'] = number_format(count($transferItem)) . ' record';
            $transferItemDetails = array();
            foreach ($transferItem as $row) {
                if ($row->transferItemDetails != null) {
                    $transferItemDetails = array_merge($transferItemDetails, $row->transferItemDetails);
                }
            }
            if (count($transferItemDetails) > 0) {
                $upload['TransferItemDetails'] = $transferItemDetails;
                $content['TransferItemDetails'] = number_format(count($transferItemDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($transferItem as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($transferItem)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($transferItem)) . ' records');
                $log = new UploadLog;
                $log->target = 'T' . STOREIDTARGET;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// TransferItem ========================================================================================================
// Kas =================================================================================================================
        /* @var $kas Kas*/
        $kas = Kas::model()->findAll($criteria);
//        $kas = new Kas;
        $upload = array();
        $content = array();
        if ($kas != null) {
            $modelName = 'Kas';
            $upload['Kas'] = $kas;
            $content['Kas'] = number_format(count($kas)) . ' record';
            $pelunasanUtangDetails = array();
            foreach ($kas as $row) {
                if (count($row->kasDetails) > 0) {
                    $pelunasanUtangDetails = array_merge($pelunasanUtangDetails, $row->kasDetails);
                }
            }
            if (count($pelunasanUtangDetails) > 0) {
                $upload['KasDetail'] = $pelunasanUtangDetails;
                $content['KasDetail'] = number_format(count($pelunasanUtangDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($kas as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($kas)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($kas)) . ' records');
                $log = new UploadLog;
                $log->target = 'T' . STOREIDTARGET;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// Kas =================================================================================================================
// Salestrans ==========================================================================================================
        $salestrans = Salestrans::model()->findAll($criteria);
        $upload = array();
        $content = array();
        if ($salestrans != null) {
            $modelName = 'Salestrans';
            $upload['Salestrans'] = $salestrans;
            $content['Salestrans'] = number_format(count($salestrans)) . ' record';
            $salestransDetails = array();
            $payments = array();
            foreach ($salestrans as $row) {
                if (count($row->salestransDetails) > 0) {
                    $salestransDetails = array_merge($salestransDetails, $row->salestransDetails);
                }
                if (count($row->payments) > 0) {
                    $payments = array_merge($payments, $row->payments);
                }
            }
            if (count($salestransDetails) > 0) {
                $upload['SalestransDetails'] = $salestransDetails;
                $content['SalestransDetails'] = number_format(count($salestransDetails)) . ' record';
            }
            if (count($payments) > 0) {
                $upload['Payment'] = $payments;
                $content['Payment'] = number_format(count($payments)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($salestrans as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($tables)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($tables)) . ' records');
                $log = new UploadLog;
                $log->target = 'T' . STOREIDTARGET;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// Salestrans ==========================================================================================================
// BeautyServices ======================================================================================================
        $pelunasanUtang = BeautyServices::model()->findAll($criteria);
        $upload = array();
        $content = array();
        if ($pelunasanUtang != null) {
            $modelName = 'BeautyServices';
            $upload['BeautyServices'] = $pelunasanUtang;
            $content['BeautyServices'] = number_format(count($pelunasanUtang)) . ' record';
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($pelunasanUtang as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($pelunasanUtang)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($pelunasanUtang)) . ' records');
                $log = new UploadLog;
                $log->target = 'T' . STOREIDTARGET;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// BeautyServices ======================================================================================================
// PelunasanUtang ======================================================================================================
        $pelunasanUtang = PelunasanUtang::model()->findAll($criteria);
        $upload = array();
        $content = array();
        if ($pelunasanUtang != null) {
            $modelName = 'PelunasanUtang';
            $upload['PelunasanUtang'] = $pelunasanUtang;
            $content['PelunasanUtang'] = number_format(count($pelunasanUtang)) . ' record';
            $pelunasanUtangDetails = array();
            foreach ($pelunasanUtang as $row) {
                if ($row->pelunasanUtang != null) {
                    $pelunasanUtangDetails = array_merge($pelunasanUtangDetails, $row->pelunasanUtang);
                }
            }
            if (count($pelunasanUtangDetails) > 0) {
                $upload['PelunasanUtangDetil'] = $pelunasanUtangDetails;
                $content['PelunasanUtangDetil'] = number_format(count($pelunasanUtangDetails)) . ' record';
            }
            $time = date('Y-m-d H:i:s');
            $subject = 'T' . STOREIDTARGET . '-' . sha1(STOREID . "^" . $modelName . "^" . $time);
            $status = mailsend($to, $to, $subject,
                json_encode(array(
                    'from' => STOREID,
                    'date' => $time,
                    'content' => array($content)
                ), JSON_PRETTY_PRINT),
                bzcompress(Encrypt(CJSON::encode($upload)), 9)
            );
            if ($status == "OK") {
//                $table = CActiveRecord::model($modelName)->tableName();
//                $r = Yii::app()->db->createCommand("
//                UPDATE $table SET up = 1 WHERE up = 0")->execute();
                foreach ($pelunasanUtang as $model) {
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $modelName)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['upload_transaksi'][$modelName] = 'success upload ' . number_format(count($pelunasanUtang)) . ' records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName,
                    'success upload ' . number_format(count($pelunasanUtang)) . ' records');
                $log = new UploadLog;
                $log->target = 'T' . STOREIDTARGET;
                $log->upload_time = $time;
                $log->content_ = CJSON::encode($upload);
                $log->subject = $subject;
                if (!$log->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Upload Log')) . CHtml::errorSummary($log));
                }
            } else {
//                $this->result['upload_transaksi'][$modelName] = 'failed upload records';
                self::echo_log('UPLOAD_TRANSAKSI', $modelName, 'failed upload records');
            }
        }
// PelunasanUtang ======================================================================================================
    }
    private function broadcast_master()
    {
        $store = Store::model()->findAll('store_kode <> :store_kode',
            array(':store_kode' => STOREID));
        foreach ($store as $target) {
            self::upload_master_cabang(STOREID, $target->store_kode);
        }
    }
    private function download_global()
    {
        $json = self::download_email(STOREGLOBAL, 'G');
        if ($json === false) {
//            $this->result['download_global'] = $this->status_error;
            self::echo_log('DOWNLOAD_GLOBAL', 'failed download', $this->status_error);
            return;
        }
        foreach ($json as $email) {
            $data = json_decode($email, true);
            foreach ($data as $k => $v) {
                foreach ($v as $row) {
                    $primarykey = CActiveRecord::model($k)->tableSchema->primaryKey;
                    $pkval = $row[$primarykey];
                    $model = CActiveRecord::model($k)->findByPk($pkval);
                    if ($model == null) {
                        $model = new $k;
                    }
                    $model->attributes = $row;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $k)) . CHtml::errorSummary($model));
                    }
                    $model->up = 1;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $k)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['download_global'][$k] = 'success download ' . number_format(count($v)) . ' records';
                self::echo_log('DOWNLOAD_GLOBAL', $k, 'success download ' . number_format(count($v)) . ' records');
            }
        }
    }
    private function download_master()
    {
        $json = self::download_email(STOREID, 'M');
        if ($json === false) {
//            $this->result['download_master'] = $this->status_error;
            self::echo_log('DOWNLOAD_MASTER', 'failed download', $this->status_error);
            return;
        }
        foreach ($json as $email) {
            $data = json_decode($email, true);
            foreach ($data as $k => $v) {
                switch ($k) {
                    case 'Beli':
                        foreach ($v as $row) {
                            Beli::save_default_price($row['barang_id'], $row['price'], $row['tax'], $row['store']);
                            /* @var $beli Beli*/
                            $beli = Beli::model()->findByAttributes(array(
                                'barang_id'=>$row['barang_id'],
                                'price'=>$row['price'],
                                'tax'=>$row['tax'],
                                'store'=>$row['store']
                            ));
                            if($beli != null){
                                $beli->up = 1;
                                if (!$beli->save()) {
                                    throw new Exception(t('save.model.fail', 'app',
                                            array('{model}' => $k)) . CHtml::errorSummary($beli));
                                }
                            }
                        }
                        break;
                    case 'Jual':
                        foreach ($v as $row) {
                            Jual::save_jual($row['barang_id'], $row['price'], $row['cost'], $row['store']);
                            /* @var $jual Jual*/
                            $jual = Jual::model()->findByAttributes(array(
                                'barang_id'=>$row['barang_id'],
                                'price'=>$row['price'],
                                'cost'=>$row['cost'],
                                'store'=>$row['store']
                            ));
                            if($jual != null){
                                $jual->up = 1;
                                if (!$jual->save()) {
                                    throw new Exception(t('save.model.fail', 'app',
                                            array('{model}' => $k)) . CHtml::errorSummary($jual));
                                }
                            }
                        }
                        break;
                    case 'Diskon':
                        foreach ($v as $row) {
                            Diskon::save_diskon($row['barang_id'], $row['status_cust_id'], $row['value'],
                                $row['store']);
                            /* @var $diskon Diskon*/
                            $diskon = Diskon::model()->findByAttributes(array(
                                'barang_id'=>$row['barang_id'],
                                'status_cust_id'=>$row['status_cust_id'],
                                'value'=>$row['value']
                            ));
                            if($diskon != null){
                                $diskon->up = 1;
                                if (!$diskon->save()) {
                                    throw new Exception(t('save.model.fail', 'app',
                                            array('{model}' => $k)) . CHtml::errorSummary($diskon));
                                }
                            }
                        }
                        break;
                    case 'Price':
                        foreach ($v as $row) {
                            Price::save_price($row['barang_id'], $row['gol_id'], $row['value'], $row['store']);
                            /* @var $price Price*/
                            $price = Price::model()->findByAttributes(array(
                                'barang_id'=>$row['barang_id'],
                                'gol_id'=>$row['gol_id'],
                                'store'=>$row['store'],
                                'value'=>$row['value']
                            ));
                            if($price != null){
                                $price->up = 1;
                                if (!$price->save()) {
                                    throw new Exception(t('save.model.fail', 'app',
                                            array('{model}' => $k)) . CHtml::errorSummary($price));
                                }
                            }
                        }
                        break;
                    case 'SysPrefs':
                        foreach ($v as $row) {
                            SysPrefs::save_value($row['name_'], $row['value_'], $row['store']);
                            /* @var $sysPrefs SysPrefs*/
                            $sysPrefs = SysPrefs::model()->findByAttributes(array(
                                'name_'=>$row['name_'],
                                'value_'=>$row['value_'],
                                'store'=>$row['store']
                            ));
                            if($sysPrefs != null){
                                $sysPrefs->up = 1;
                                if (!$sysPrefs->save()) {
                                    throw new Exception(t('save.model.fail', 'app',
                                            array('{model}' => $k)) . CHtml::errorSummary($sysPrefs));
                                }
                            }
                        }
                        break;
                    case 'GrupAttr':
                        foreach ($v as $row) {
                            GrupAttr::save_grup_attr($row['vat'], $row['tax'], $row['coa_jual'], $row['coa_sales_disc'],
                                $row['coa_sales_hpp'],$row['coa_purchase'], $row['coa_purchase_disc'], $row['coa_purchase_return'],
                                $row['store'], $row['up'], $row['grup_id']
                            );
                            /* @var $grupAttr GrupAttr*/
                            $grupAttr = GrupAttr::model()->findByAttributes(array(
                                'vat'=>$row['vat'],
                                'tax'=>$row['tax'],
                                'coa_jual'=>$row['coa_jual'],
                                'coa_sales_disc'=>$row['coa_sales_disc'],
                                'coa_sales_hpp'=>$row['coa_sales_hpp'],
                                'coa_purchase'=>$row['coa_purchase'],
                                'coa_purchase_disc'=>$row['coa_purchase_disc'],
                                'coa_purchase_return'=>$row['coa_purchase_return'],
                                'store'=>$row['store'],
                                'grup_id'=>$row['grup_id']
                            ));
                            if($grupAttr != null){
                                $grupAttr->up = 1;
                                if (!$grupAttr->save()) {
                                    throw new Exception(t('save.model.fail', 'app',
                                            array('{model}' => $k)) . CHtml::errorSummary($grupAttr));
                                }
                            }
                        }
                        break;
                    default:
                        foreach ($v as $row) {
                            $primarykey = CActiveRecord::model($k)->tableSchema->primaryKey;
                            if (in_array($k, $this->skip_primary)) {
                                $model = null;
                                $row[$primarykey] = null;
                            } else {
                                $pkval = $row[$primarykey];
                                $model = CActiveRecord::model($k)->findByPk($pkval);
                            }
                            if ($model == null) {
                                $model = new $k;
                            }
                            $model->attributes = $row;
                            if (!$model->save()) {
                                throw new Exception(t('save.model.fail', 'app',
                                        array('{model}' => $k)) . CHtml::errorSummary($model));
                            }
                            unset($row);
                            $row['up'] = MARKAFTERDOWNLOAD ? 1 : 0;
                            $model->attributes = $row;
                            if (!$model->save()) {
                                throw new Exception(t('save.model.fail', 'app',
                                        array('{model}' => $k)) . CHtml::errorSummary($model));
                            }
                        }
//                        $this->result['download_master'][$k] = 'success download ' . number_format(count($v)) . ' records';
                        self::echo_log('DOWNLOAD_MASTER', $k,
                            'success download ' . number_format(count($v)) . ' records');
                        break;
                }
//                $this->result['download_master'][$k] = 'success download ' . number_format(count($v)) . ' records';
                self::echo_log('DOWNLOAD_MASTER', $k, 'success download ' . number_format(count($v)) . ' records');
            }
        }
    }
    private function download_transaksi()
    {
        $remove_beautyservices = $remove_kas = $remove_gl =
        $remove_bank_trans = $remove_stock_move = $remove_pelunasan_utang = array();
        $json = self::download_email(STOREID, 'T');
        if ($json === false) {
//            $this->result['download_transaksi'] = $this->status_error;
            self::echo_log('DOWNLOAD_TRANSAKSI', 'failed download', $this->status_error);
            return;
        }
        foreach ($json as $email) {
            $data = json_decode($email, true);
            foreach ($data as $k => $v) {
                foreach ($v as $row) {
                    if ($k == 'BeautyServices') {
                        if (!in_array($row['salestrans_details'], $remove_beautyservices)) {
                            $delete = Yii::app()->db->createCommand("
                            DELETE FROM nscc_beauty_services WHERE salestrans_details = :salestrans_details");
                            $delete->execute(array(':salestrans_details' => $row['salestrans_details']));
                            $remove_beautyservices[] = $row['salestrans_details'];
                        }
                    } elseif ($k == 'Kas') {
                        if (!in_array($row['kas_id'], $remove_kas)) {
                            KasDetail::model()->deleteAll('kas_id = :kas_id', array(':kas_id' => $row['kas_id']));
//                            $type = $row['arus'] == 1 ? CASHIN : CASHOUT;
//                            $type_no = $row['kas_id'];
//                            BankTrans::model()->deleteAll('type_ = :type AND trans_no = :type_no', array(':type' => $type, ':type_no' => $type_no));
//                            GlTrans::model()->deleteAll('type = :type AND type_no = :type_no', array(':type' => $type, ':type_no' => $type_no));
                            $remove_kas[] = $row['kas_id'];
                        }
                    } elseif ($k == 'PelunasanUtang') {
                        if (!in_array($row['pelunasan_utang_id'], $remove_pelunasan_utang)) {
                            PelunasanUtangDetil::model()->deleteAll('pelunasan_utang_id = :pelunasan_utang_id',
                                array(':pelunasan_utang_id' => $row['pelunasan_utang_id']));
//                            $type = PELUNASANUTANG;
//                            $type_no = $row['pelunasan_utang_id'];
//                            BankTrans::model()->deleteAll('type_ = :type AND trans_no = :type_no', array(':type' => $type, ':type_no' => $type_no));
//                            GlTrans::model()->deleteAll('type = :type AND type_no = :type_no', array(':type' => $type, ':type_no' => $type_no));
                            $remove_pelunasan_utang[] = $row['kas_id'];
                        }
                    } elseif ($k == 'GlTrans') {
                        $type = $row['type'];
                        $type_no = $row['type_no'];
                        if ((isset($remove_gl[$type]) && !in_array($row['type_no'],
                                    $remove_gl[$type])) || !isset($remove_gl[$type])
                        ) {
                            GlTrans::model()->deleteAll('type = :type AND type_no = :type_no',
                                array(':type' => $type, ':type_no' => $type_no));
                            $remove_gl[$type][] = $row['type_no'];
                        }
                    } elseif ($k == 'BankTrans') {
                        $type = $row['type_'];
                        $type_no = $row['trans_no'];
                        if ((isset($remove_bank_trans[$type]) && !in_array($row['trans_no'],
                                    $remove_bank_trans[$type])) || !isset($remove_bank_trans[$type])
                        ) {
                            BankTrans::model()->deleteAll('type_ = :type AND trans_no = :type_no',
                                array(':type' => $type, ':type_no' => $type_no));
                            $remove_bank_trans[$type][] = $row['trans_no'];
                        }
                    } elseif ($k == 'StockMoves') {
                        $type = $row['type_no'];
                        $type_no = $row['trans_no'];
                        if ((isset($remove_Stock_Moves[$type]) && !in_array($row['trans_no'],
                                    $remove_Stock_Moves[$type])) || !isset($remove_Stock_Moves[$type])
                        ) {
                            StockMoves::model()->deleteAll('type_no = :type AND trans_no = :type_no',
                                array(':type' => $type, ':type_no' => $type_no));
                            $remove_Stock_Moves[$type][] = $row['trans_no'];
                        }
                    } elseif ($k == 'Refs') {
                        Yii::app()->db->createCommand("
                        DELETE FROM nscc_refs WHERE type_no = :type_no AND type_ = :type_")
                            ->execute(array(':type_' => $row['type_'], ':type_no' => $row['type_no']));
                    }
                    $primarykey = CActiveRecord::model($k)->tableSchema->primaryKey;
                    if (in_array($k, $this->skip_primary)) {
                        $model = null;
                        $row[$primarykey] = null;
                    } else {
                        $pkval = $row[$primarykey];
                        $model = CActiveRecord::model($k)->findByPk($pkval);
                    }
                    if ($model == null) {
                        $model = new $k;
                    }
                    $model->attributes = $row;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $k)) . CHtml::errorSummary($model));
                    }
                    unset($row);
                    $row['up'] = MARKAFTERDOWNLOAD ? 1 : 0;
                    $model->attributes = $row;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => $k)) . CHtml::errorSummary($model));
                    }
                }
//                $this->result['download_transaksi'][$k] = 'success download ' . number_format(count($v)) . ' records';
                self::echo_log('DOWNLOAD_TRANSAKSI', $k, 'success download ' . number_format(count($v)) . ' records');
            }
        }
    }
    private function download_email($imapmainbox, $type_ = null)
    {
        $json = array();
        $messagestatus = "ALL";
        $imapaddress = IMAPADDRESS;
        $hostname = $imapaddress . ($type_ == 'G' ? '' : $type_) . $imapmainbox;
        $connection = @imap_open($hostname, $this->username, $this->password);
        if ($connection === false) {
            $this->status_error = 'Cannot connect to Gmail: ' . imap_last_error();
            return false;
        }
        $check = imap_check($connection);
        if ($check->Nmsgs == 0) {
            @imap_close($connection);
            return false;
        }
        $emails = imap_search($connection, $messagestatus);
        if ($emails) {
            foreach ($emails as $email_number) {
                $header = imap_fetch_overview($connection, $email_number, 0);
                if (empty($header)) {
                    self::echo_log('DONWLOAD_EMAIL', 'HEADER_EMPTY', var_export($header, true));
                    continue;
                }
                $subject = $header[0]->subject;
                $date = strtotime($header[0]->date);
                $date2 = time();
                $subTime = $date2 - $date;
                $h = $subTime / (60 * 60 * 24);
                if ($h > DAYSBEFOREDELSYNC) {
                    imap_delete($connection, $email_number, 1);
                    continue;
                }
                $criteria = new CDbCriteria;
                $criteria->addCondition("type_ = :type_ AND subject = :subject");
                $criteria->params = array(':type_' => $type_, ':subject' => $subject);
                $count = Sync::model()->count($criteria);
                if ($count > 0) {
                    continue;
                }
                $message = imap_fetchbody($connection, $email_number, 1.1);
                if ($message == "") {
                    $message = imap_fetchbody($connection, $email_number, 1);
                }
                $structure = imap_fetchstructure($connection, $email_number);
                $attachments = array();
                if (isset($structure->parts) && count($structure->parts)) {
                    for ($i = 0; $i < count($structure->parts); $i++) {
                        $attachments[$i] = array(
                            'is_attachment' => false,
                            'filename' => '',
                            'name' => '',
                            'attachment' => ''
                        );
                        if ($structure->parts[$i]->ifdparameters) {
                            foreach ($structure->parts[$i]->dparameters as $object) {
                                if (strtolower($object->attribute) == 'filename') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['filename'] = $object->value;
                                }
                            }
                        }
                        if ($structure->parts[$i]->ifparameters) {
                            foreach ($structure->parts[$i]->parameters as $object) {
                                if (strtolower($object->attribute) == 'name') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['name'] = $object->value;
                                }
                            }
                        }
                        if ($attachments[$i]['is_attachment']) {
                            $attachments[$i]['attachment'] = imap_fetchbody($connection, $email_number, $i + 1);
                            if ($structure->parts[$i]->encoding == 3) { // 3 = BASE64
                                $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                            } elseif ($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
                                $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                            }
                        }
                    }
                }
                if (count($attachments) != 0) {
                    foreach ($attachments as $at) {
                        if ($at['is_attachment'] == 1) {
                            $json[] = Decrypt(bzdecompress($at['attachment']));
                            $sync = new Sync;
                            $sync->type_ = $type_;
                            $sync->subject = $subject;
                            $sync->message = $message;
                            if (!$sync->save()) {
                                throw new Exception(t('save.model.fail', 'app',
                                        array('{model}' => 'Sync')) . CHtml::errorSummary($sync));
                            }
                        }
                    }
                }
            }
        }
        imap_expunge($connection);
        imap_close($connection);
        return $json;
    }
    private function echo_log($cat, $model, $note)
    {
        echo json_encode(
                array(
                    'time' => date("Y-m-d H:i:s"),
                    'event' => $cat,
                    'model' => $model,
                    'note' => $note
                )) . PHP_EOL;
    }
    private function remainder()
    {
        $rec = app()->db->createCommand("SELECT nti.store,ns.supplier_name,
        DATE_FORMAT(nti.tgl,'%d %b %Y') Date,DATE_FORMAT(nti.tgl_jatuh_tempo,'%d %b %Y') DueDate,
        DATEDIFF(nti.tgl_jatuh_tempo, NOW()) remainDays,nti.doc_ref,nti.doc_ref_other,nti.total,
        nti.total - (IF (SUM(npud.kas_dibayar) IS NULL,0,SUM(npud.kas_dibayar))) AS 'Remain'
        FROM nscc_transfer_item AS nti
        LEFT JOIN nscc_pelunasan_utang_detil AS npud ON nti.transfer_item_id = npud.transfer_item_id
        INNER JOIN nscc_supplier AS ns ON nti.supplier_id = ns.supplier_id
        WHERE nti.lunas = 0 AND DATEDIFF(nti.tgl_jatuh_tempo, NOW()) < :start
        GROUP BY nti.transfer_item_id
        HAVING Remain > 0");
        $result = $rec->queryAll(true, array(':start' => REMAINDER_START));
        if (count($result) == 0) {
            return;
        }
        $stat = SysPrefs::model()->find('name_ = :name AND store =:store',
            array(':name' => 'last_remainder', ':store' => STOREID));
        $last_remainder = $stat->value_;
        $dStart = new DateTime($last_remainder);
        $dEnd = new DateTime();
        $dDiff = $dStart->diff($dEnd);
        //echo $dDiff->format('%R'); // use for point out relation: smaller/greater
        $days = $dDiff->days;
        if ($days < REMAINDER_INTERVAL) {
            return;
        }
        $sum_Remain = array_sum(array_column($result, 'Remain'));
        $sum_total = array_sum(array_column($result, 'total'));
        $body = "<html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
</head>
<body>
<p>Dear Bapak Ferro,</p>
<p>Berikut data tagihan yang mendekati jatuh tempo.</p>
<table style='font-family: verdana,arial,sans-serif;font-size:11px;color:#333333;border: 1px solid #666666;' ><tr>
<th style='border: 1px solid #666666;' bgcolor='#dedede'>Supplier</th>
<th style='border: 1px solid #666666;' bgcolor='#dedede'>Date</th>
<th style='border: 1px solid #666666;' bgcolor='#dedede'>Due Date</th>
<th style='border: 1px solid #666666;' bgcolor='#dedede' align='right'>Remain Days</th>
<th style='border: 1px solid #666666;' bgcolor='#dedede'>Invoice No.</th>
<th style='border: 1px solid #666666;' bgcolor='#dedede' align='right'>Total</th>
<th style='border: 1px solid #666666;' bgcolor='#dedede' align='right'>Remain</th></tr>";
        foreach ($result as $row) {
            $body .= "<tr>
<td style='border: 1px solid #666666;' bgcolor='#ffffff'>" . $row['supplier_name'] . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff'>" . $row['Date'] . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff'>" . $row['DueDate'] . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff' align='right'>" . $row['remainDays'] . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff'>" . $row['doc_ref_other'] . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff' align='right'>" . number_format($row['total'], 2) . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff' align='right'>" . number_format($row['Remain'],
                    2) . "</td></tr>";
        }
        $body .= "<tr>
<td style='border: 1px solid #666666;' bgcolor='#ffffff'>TOTAL</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff' colspan='4'></td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff' align='right'>" . number_format($sum_total, 2) . "</td>
<td style='border: 1px solid #666666;' bgcolor='#ffffff' align='right'>" . number_format($sum_Remain, 2) . "</td></tr>";
        $body .= "</table><p>Terima Kasih</p>
</body>
</html>";
//        $body = $this->render('Remainder', array('dp' => $dataProvider, 'total_remain' => $sum_Remain), true);
        $send = mailsend_remainder(REMAINDER_EMAIL, yiiparam('Username'),
            'REMAINDER TAGIHAN HUTANG NWIS ' . STOREID . ' ' . date('d/m/Y'), $body);
        if ($send == "OK") {
            $stat->value_ = date('Y-m-d');
            $stat->save();
            self::echo_log('REMAINDER', 'SEND_REMAINDER',
                'success remainder ' . number_format(count($rec)) . ' records');
//            $this->result['send_remainder'] = 'success remainder ' . number_format(count($rec)) . ' records';
        } else {
//            $this->result['send_remainder'] = 'failed remainder';
            self::echo_log('REMAINDER', 'SEND_REMAINDER', 'failed remainder');
        }
    }
} 
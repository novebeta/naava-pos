<?php

Yii::import('application.models._base.BaseBeauty');
class Beauty extends BaseBeauty
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->isNewRecord && $this->beauty_id == null) {
            $this->beauty_id = U::generate_primary_key(RBEAUTY);
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        return parent::beforeValidate();
    }
}
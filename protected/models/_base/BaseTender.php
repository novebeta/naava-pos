<?php

/**
 * This is the model base class for the table "{{tender}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Tender".
 *
 * Columns in table "{{tender}}" available as properties of the model,
 * followed by relations of table "{{tender}}" available as properties of the model.
 *
 * @property string $tender_id
 * @property string $tgl
 * @property string $doc_ref
 * @property string $user_id
 * @property string $tdate
 * @property double $total
 * @property string $store
 * @property integer $up
 *
 * @property Printz[] $printzs
 * @property TenderDetails[] $tenderDetails
 */
abstract class BaseTender extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{tender}}';
	}

	public static function representingColumn() {
		return 'tgl';
	}

	public function rules() {
		return array(
			array('tender_id, tgl, doc_ref, user_id, tdate, store', 'required'),
			array('up', 'numerical', 'integerOnly'=>true),
			array('total', 'numerical'),
			array('tender_id, doc_ref, user_id', 'length', 'max'=>50),
			array('store', 'length', 'max'=>20),
			array('total, up', 'default', 'setOnEmpty' => true, 'value' => null),
			array('tender_id, tgl, doc_ref, user_id, tdate, total, store, up', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'printzs' => array(self::HAS_MANY, 'Printz', 'tender_id'),
			'tenderDetails' => array(self::HAS_MANY, 'TenderDetails', 'tender_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'tender_id' => Yii::t('app', 'Tender'),
			'tgl' => Yii::t('app', 'Tgl'),
			'doc_ref' => Yii::t('app', 'Doc Ref'),
			'user_id' => Yii::t('app', 'User'),
			'tdate' => Yii::t('app', 'Tdate'),
			'total' => Yii::t('app', 'Total'),
			'store' => Yii::t('app', 'Store'),
			'up' => Yii::t('app', 'Up'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('tender_id', $this->tender_id, true);
		$criteria->compare('tgl', $this->tgl, true);
		$criteria->compare('doc_ref', $this->doc_ref, true);
		$criteria->compare('user_id', $this->user_id, true);
		$criteria->compare('tdate', $this->tdate, true);
		$criteria->compare('total', $this->total);
		$criteria->compare('store', $this->store, true);
		$criteria->compare('up', $this->up);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}
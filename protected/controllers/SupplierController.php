<?php
class SupplierController extends GxController
{
    public function actionCreate()
    {
        $model = new Supplier;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['account_code'];
            $status = true;
            if (U::account_in_gl_trans($id)) {
                $status = false;
                $msg = t('coa.fail.use.gl','app',array('{coa}'=>$id));
            } elseif (U::account_used_supplier($id)) {
                $status = false;
                $msg = t('coa.fail.use.model','app',array('{coa}'=>$id,'{model}'=>'Supplier'));
            }
            if (!$status) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            }
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Supplier'][$k] = $v;
            }
            $model->attributes = $_POST['Supplier'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->supplier_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Supplier');
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['account_code'];
            $status = true;
            if (U::account_in_gl_trans($id)) {
                $status = false;
                $msg = t('coa.fail.use.gl','app',array('{coa}'=>$id));
            } elseif (U::account_used_supplier($id)) {
                $status = false;
                $msg = t('coa.fail.use.model','app',array('{coa}'=>$id,'{model}'=>'Supplier'));
            }
            if (!$status) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            }
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Supplier'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Supplier'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->supplier_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->supplier_id));
            }
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = Supplier::model()->findAll($criteria);
        $total = Supplier::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}
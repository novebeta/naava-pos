<?php

Yii::import('application.models._base.BaseDokter');
class Dokter extends BaseDokter
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->isNewRecord && $this->dokter_id == null) {
            $this->dokter_id = U::generate_primary_key(RDOKTER);
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        return parent::beforeValidate();
    }
}
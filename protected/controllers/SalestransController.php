<?php
Yii::import('application.components.Reference');
Yii::import('application.components.U');
Yii::import('application.components.PrintReceipt');
Yii::import('application.components.GL');
class SalestransController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            if (Tender::is_exist($_POST['tgl'])) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Tender Declaration already created'));
                Yii::app()->end();
            }
            $gl = new GL();
            $msg = "Stored data failed.";
            $detils = CJSON::decode($_POST['detil']);
            $payments = CJSON::decode($_POST['payment']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new Salestrans;
                $ref = new Reference();
                $docref = $ref->get_next_reference(PENJUALAN);
                if ($_POST['log'] == 0) {
                    $_POST['store'] = STOREID;
                }
                foreach ($_POST as $k => $v) {
                    //if ($k == 'detil' || $k = 'payment') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Salestrans'][$k] = $v;
                }
//                $_POST['Salestrans']['salestrans_id'] = "1";
                $_POST['Salestrans']['type_'] = 1;
                $_POST['Salestrans']['doc_ref'] = $docref;
//                $_POST['Salestrans']['tgl'] = new CDbExpression('NOW()');
                $model->attributes = $_POST['Salestrans'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales')) . CHtml::errorSummary($model));
//                $transaction->commit();
//                Yii::app()->end();
                foreach ($payments as $pay) {
                    $amount = get_number($pay['amount']);
                    $kembali = 0;
                    if ($pay['bank_id'] == Bank::get_bank_cash_id()) {
                        if ($model->kembali > 0) {
                            $kembali = $model->kembali;
                        }
                    }
                    Payment::add_payment($pay['bank_id'], $model->salestrans_id, $pay['card_number'], $amount, $pay['card_id'], $kembali);
                }
                //GL BANK (sesuai cara bayar)
                //        VAT
                //        Diskon Faktur
                foreach ($model->payments as $pay) {
                    if ($pay->amount != 0) {
                        if ($pay->bank->is_bank_cash()) {
                            $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $pay->bank->account_code,
                                "Sales $docref", "Sales $docref", $pay->amount - $pay->kembali, 0, $model->store);
                        } else {
                            if ($pay->card == null) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales')) . 'Card type must select!');
                            }
                            $fee = ($pay->card->persen_fee / 100) * $pay->amount;
                            $fee = round($fee,2);
                            $edc = $pay->amount - $fee;
                            $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $pay->bank->account_code,
                                "Sales $docref", "Sales $docref", $edc, 0, $model->store);
                            $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, COA_FEE_CARD,
                                "Sales $docref", "Sales $docref", $fee, 0, $model->store);
                        }
                    }
                }
                if ($model->discrp != 0) {
//              DISKON VOUCHER
                    $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, COA_VOUCHER,
                        "Sales $docref", "Sales $docref", $model->discrp, 1);
                }
                if ($model->vat != 0) {
                    $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, COA_VAT,
                        "Sales $docref", "Sales $docref", -$model->vat, 1);
                }
                foreach ($detils as $detil) {
                    $salestrans_detail = new SalestransDetails;
                    $_POST['SalestransDetails']['barang_id'] = $detil['barang_id'];
                    $_POST['SalestransDetails']['qty'] = get_number($detil['qty']);
                    $_POST['SalestransDetails']['price'] = get_number($detil['price']);
                    $_POST['SalestransDetails']['disc'] = get_number($detil['disc']);
                    $_POST['SalestransDetails']['discrp'] = get_number($detil['discrp']);
                    $_POST['SalestransDetails']['ketpot'] = $detil['ketpot'];
                    $_POST['SalestransDetails']['vat'] = get_number($detil['vat']);
                    $_POST['SalestransDetails']['vatrp'] = get_number($detil['vatrp']);
                    $_POST['SalestransDetails']['total_pot'] = get_number($detil['total_pot']);
                    $_POST['SalestransDetails']['total'] = get_number($detil['total']);
                    $_POST['SalestransDetails']['bruto'] = get_number($detil['bruto']);
                    $_POST['SalestransDetails']['disc_name'] = $detil['disc_name'];
                    $_POST['SalestransDetails']['disc1'] = get_number($detil['disc1']);
                    $_POST['SalestransDetails']['discrp1'] = get_number($detil['discrp1']);
                    $_POST['SalestransDetails']['salestrans_id'] = $model->salestrans_id;
                    $salestrans_detail->attributes = $_POST['SalestransDetails'];
                    if (!$salestrans_detail->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales Detail')) . CHtml::errorSummary($salestrans_detail));
                    $salestrans_detail->save_beauty_tips($detil['beauty_id'], $detil['beauty2_id']);
                    $tipe_jual = "Sales " . $salestrans_detail->barang->grup->nama_grup;
                    //GL Diskon item
                    if ($salestrans_detail->total_pot != 0) {
                        $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $salestrans_detail->barang->get_coa_sales_disc($model->store),
                            $tipe_jual, $tipe_jual, $salestrans_detail->total_pot, 1);
                    }
                    //GL Penjualan sesuai grup barang
                    if ($salestrans_detail->bruto != 0) {
                        $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $salestrans_detail->barang->get_coa_sales($model->store),
                            $tipe_jual, $tipe_jual, -$salestrans_detail->bruto, 1,$model->store);
                    }
//                    if ($model->log == 0) {
                    if ($salestrans_detail->barang->grup->kategori->is_have_stock()) {
                        //------------------------------ khusus barang non jasa ------------------------------------
                        $saldo_stock = StockMoves::get_saldo_item($salestrans_detail->barang_id,$model->store);
                        if ($saldo_stock < $salestrans_detail->qty) {
                            throw new Exception(t('saldo.item.fail',
                                'app', array('{item}' => $salestrans_detail->barang->kode_barang,
                                    '{h}' => $saldo_stock, '{r}' => $salestrans_detail->qty)));
                        }
                        U::add_stock_moves(PENJUALAN, $model->salestrans_id, $model->tgl,
                            $salestrans_detail->barang_id, -$salestrans_detail->qty, $model->doc_ref,
                            $salestrans_detail->barang->get_cost($model->store), $model->store);
                        // Hitung HPP
                        $cost = $salestrans_detail->barang->get_cost($model->store);
                        $hpp = $salestrans_detail->qty * $cost;
                        $salestrans_detail->cost = $cost;
                        $salestrans_detail->hpp = $hpp;
                        if (!$salestrans_detail->save())
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales Hpp Detail')) . CHtml::errorSummary($salestrans_detail));
                        //GL HPP
                        //  Persediaan
                        $coa_sales_hpp = $salestrans_detail->barang->get_coa_sales_hpp($model->store);
                        if ($hpp != 0 && $coa_sales_hpp != null) {
                            $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $coa_sales_hpp,
                                $tipe_jual, $tipe_jual, $hpp, 0);
                            $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, COA_PERSEDIAAN,
                                $tipe_jual, $tipe_jual, -$hpp, 0);
                        }
                    }
//                    }
                }
                $gl->validate();
                $ref->save(PENJUALAN, $model->salestrans_id, $docref);
                $prt = new PrintReceipt($model);
                $msg = $prt->buildTxt();
                $transaction->commit();
//                if ($model->log == 0) {
//                    $prt = new PrintReceipt($model);
//                    $msg = $prt->buildTxt();
//                } else {
//                    $msg = t('save.success', 'app');
//                }
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Salestrans');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Salestrans'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Salestrans'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->salestrans_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->salestrans_id));
            }
        }
    }

    public function actionIndex()
    {
        $this->renderJsonArr(Salestrans::get_trans($_POST['tgl']));
    }

    public function actionBeautyService()
    {
        if (Yii::app()->request->isPostRequest) {
            $tgl = $_POST['tgl'];
            $arr = Salestrans::list_beuty_service($tgl);
            $this->renderJsonArr($arr);
            Yii::app()->end();
        } else
            throw new CHttpException(403,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionHistory()
    {
        $this->renderJsonArr(Salestrans::get_trans_history($_POST['tgl']));
    }

    public function actionPrint()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $sls = Salestrans::model()->findByPk($_POST['id']);
            $prt = new PrintReceipt($sls);
            echo CJSON::encode(array(
                'success' => $sls != null,
                'msg' => $prt->buildTxt()));
            Yii::app()->end();
        }
    }
}
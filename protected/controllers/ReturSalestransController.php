<?php
Yii::import('application.components.Reference');
Yii::import('application.components.U');
class ReturSalestransController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            if (Tender::is_exist($_POST['tgl'])) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Tender Declaration already created'));
                Yii::app()->end();
            }
            Yii::import('application.components.GL');
            $gl = new GL();
            $msg = "Stored data failed.";
            $detils = CJSON::decode($_POST['detil']);
            $payments = CJSON::decode($_POST['payment']);
            $_POST['store'] = STOREID;
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new Salestrans;
                $ref = new Reference();
                $docref = $ref->get_next_reference(RETURJUAL);
                foreach ($_POST as $k => $v) {
//                    if ($k == 'detil') continue;
                    if (is_angka($v) && $v != null && $v != "card_number" && $v != "printed"
                        && $v != "override" && $v != "audit" && $v != "doc_ref_sales"
                    ) {
                        $v = -get_number($v);
                    }
                    $_POST['Salestrans'][$k] = $v;
                }
                $_POST['Salestrans']['doc_ref'] = $docref;
                $_POST['Salestrans']['type_'] = -1;
//                $_POST['Salestrans']['tgl'] = new CDbExpression('NOW()');
                $model->attributes = $_POST['Salestrans'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Return Sales')) . CHtml::errorSummary($model));
                foreach ($payments as $pay) {
                    $amount = -get_number($pay['amount']);
                    $kembali = 0;
                    if ($pay['bank_id'] == Bank::get_bank_cash_id()) {
                        if ($model->kembali > 0) {
                            $kembali = $model->kembali;
                        }
                    }
                    Payment::add_payment($pay['bank_id'], $model->salestrans_id, $pay['card_number'],
                        $amount, $pay['card_id'], $kembali);
                }
//                Payment::add_payment($_POST['Salestrans']['bank_id'], $model->salestrans_id, '', $model->total);
                //GL BANK (sesuai cara bayar)
                //        VAT
                //        Diskon Faktur
                foreach ($model->payments as $pay) {
                    if ($pay->amount != 0) {
                        if ($pay->bank->is_bank_cash()) {
                            $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, $pay->bank->account_code,
                                "Retur Sales $docref", "Retur Sales $docref", $pay->amount, 0, $model->store);
                        } else {
                            if ($pay->card == null) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Return Sales')) . 'Card type must select!');
                            }
                            $fee = ($pay->card->persen_fee / 100) * $pay->amount;
                            $fee = round($fee,2);
                            $edc = $pay->amount - $fee;
                            $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, $pay->bank->account_code,
                                "Retur Sales $docref", "Retur Sales $docref", $edc, 0, $model->store);
                            $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, COA_FEE_CARD,
                                "Retur Sales $docref", "Retur Sales $docref", $fee, 0, $model->store);
                        }
                    }
                }
                if ($model->discrp != 0) {
//              DISKON VOUCHER
                    $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, COA_VOUCHER,
                        "Retur Sales $docref", "Retur Sales $docref", $model->discrp, 1);
                }
                if ($model->vat != 0) {
                    $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, COA_VAT,
                        "Retur Sales $docref", "Retur Sales $docref", -$model->vat, 1);
                }
                foreach ($detils as $detil) {
                    $salestrans_detail = new SalestransDetails;
                    $_POST['SalestransDetails']['barang_id'] = $detil['barang_id'];
                    $_POST['SalestransDetails']['qty'] = -get_number($detil['qty']);
                    $_POST['SalestransDetails']['price'] = get_number($detil['price']);
                    $_POST['SalestransDetails']['disc'] = -get_number($detil['disc']);
                    $_POST['SalestransDetails']['discrp'] = -get_number($detil['discrp']);
                    $_POST['SalestransDetails']['ketpot'] = $detil['ketpot'];
                    $_POST['SalestransDetails']['beauty_id'] = null;
                    $_POST['SalestransDetails']['vat'] = -get_number($detil['vat']);
                    $_POST['SalestransDetails']['vatrp'] = -get_number($detil['vatrp']);
                    $_POST['SalestransDetails']['total_pot'] = -get_number($detil['total_pot']);
                    $_POST['SalestransDetails']['total'] = -get_number($detil['total']);
                    $_POST['SalestransDetails']['bruto'] = -get_number($detil['bruto']);
                    $_POST['SalestransDetails']['disc_name'] = $detil['disc_name'];
                    $_POST['SalestransDetails']['disc1'] = get_number($detil['disc1']);
                    $_POST['SalestransDetails']['discrp1'] = -get_number($detil['discrp1']);
                    $_POST['SalestransDetails']['salestrans_id'] = $model->salestrans_id;
                    $salestrans_detail->attributes = $_POST['SalestransDetails'];
                    if (!$salestrans_detail->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Return Sales Detail')) . CHtml::errorSummary($salestrans_detail));
//                    $salestrans_detail->save_beauty_tips();
                    $tipe_jual = "Retur Sales " . $salestrans_detail->barang->grup->nama_grup;
                    //GL Diskon item
                    if ($salestrans_detail->total_pot != 0) {
                        $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, $salestrans_detail->barang->get_coa_sales_disc($model->store),
                            $tipe_jual, $tipe_jual, $salestrans_detail->total_pot, 1, $model->store);
                    }
                    //GL Penjualan sesuai grup barang
                    if ($salestrans_detail->bruto != 0) {
                        $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, COA_SALES_RETURN,
                            $tipe_jual, $tipe_jual, -$salestrans_detail->bruto, 1, $model->store);
                    }
                    if ($salestrans_detail->barang->grup->kategori->is_have_stock()) {
                        U::add_stock_moves(RETURJUAL, $model->salestrans_id, $model->tgl,
                            $salestrans_detail->barang_id, -$salestrans_detail->qty, $model->doc_ref,
                            $salestrans_detail->barang->get_cost($model->store), $model->store);
                        // Hitung HPP
                        $cost = $salestrans_detail->barang->get_cost($model->store);
                        $hpp = $salestrans_detail->qty * $cost;
                        $salestrans_detail->cost = $cost;
                        $salestrans_detail->hpp = $hpp;
                        if (!$salestrans_detail->save())
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales Hpp Detail')) . CHtml::errorSummary($salestrans_detail));
                        //GL HPP
                        //  Persediaan
                        if ($hpp != 0) {
                            $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, $salestrans_detail->barang->get_coa_sales_hpp($model->store),
                                $tipe_jual, $tipe_jual, $hpp, 0, $model->store);
                            $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, COA_PERSEDIAAN,
                                $tipe_jual, $tipe_jual, -$hpp, 0, $model->store);
                        }
                    }
                }
                $gl->validate();
                $ref->save(RETURJUAL, $model->salestrans_id, $docref);
                $prt = new PrintReceipt($model);
                $msg = $prt->buildTxt();
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionIndex()
    {
        $this->renderJsonArr(Salestrans::get_retursales_trans($_POST['tgl']));
    }
    public function actionDetail()
    {
//        $criteria = new CDbCriteria();
//        $criteria->select = "salestrans_details,barang_id,salestrans_id,-qty qty,-disc disc,-discrp discrp,ketpot,-vat vat,
//        -vatrp vatrp,-bruto bruto,-total total,-total_pot total_pot,price,jasa_dokter,dokter_id,final,disc_name";
//        $criteria->addCondition('salestrans_id = :salestrans_id');
//        $criteria->params = array(":salestrans_id" => $_POST['salestrans_id']);
//        $model = SalestransDetails::model()->findAll($criteria);
//        $total = SalestransDetails::model()->count($criteria);
//        $this->renderJson($model, $total);
        if (Yii::app()->request->isPostRequest) {
            $this->renderJsonArr(SalestransDetails::get_retursales_trans_details($_POST['salestrans_id']));
        } else
            throw new CHttpException(403,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
}
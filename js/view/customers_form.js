jun.CustomersWin = Ext.extend(Ext.Window, {
    title: 'Form Customer',
    modez: 1,
    width: 400,
    height: 555,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'padding: 10px',
                id: 'form-Customers',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Customers No.',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_customer',
                        id: 'no_customerid',
                        ref: '../no_customer',
                        maxLength: 15,
                        allowBlank: true,
                        anchor: '100%',
                        readOnly: true
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Customers Name',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_customer',
                        id: 'nama_customerid',
                        ref: '../nama_customer',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Birthplace',
                        hideLabel: false,
                        //hidden:true,
                        name: 'tempat_lahir',
                        id: 'tempat_lahirid',
                        ref: '../tempat_lahir',
                        maxLength: 20,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_lahir',
                        fieldLabel: 'Birthday',
                        name: 'tgl_lahir',
                        id: 'tgl_lahirid',
                        format: 'd M Y',
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        ref: '../age',
                        fieldLabel: 'Age',
                        //allowBlank: ,
                        value: 0,
                        readOnly: true,
                        anchor: '100%'
                    },
                    new jun.cmbSex({
                        fieldLabel: 'Sex',
                        anchor: '100%'
                    }),
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Email',
                        hideLabel: false,
                        //hidden:true,
                        name: 'email',
                        id: 'emailid',
                        ref: '../email',
                        maxLength: 30,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        editable: false,
//                        typeAhead: true,
                        ref: '../status',
                        triggerAction: 'all',
                        lastQuery: '',
//                        lazyRender: true,
                        mode: 'local',
//                        forceSelection: true,
                        fieldLabel: 'Status',
                        store: jun.rztStatusCustCmp,
                        hiddenName: 'status_cust_id',
                        valueField: 'status_cust_id',
                        displayField: 'nama_status',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        editable: false,
//                        typeAhead: true,
                        ref: '../negara',
                        triggerAction: 'all',
                        lastQuery: '',
//                        lazyRender: true,
                        mode: 'local',
//                        forceSelection: true,
                        fieldLabel: 'Country',
                        store: jun.rztNegaraCmp,
                        hiddenName: 'negara_id',
                        valueField: 'negara_id',
                        displayField: 'nama_negara',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        editable: false,
//                        typeAhead: true,
                        ref: '../provinsi',
                        triggerAction: 'all',
                        lastQuery: '',
//                        lazyRender: true,
                        mode: 'local',
//                        forceSelection: true,
                        fieldLabel: 'Province',
                        store: jun.rztProvinsiCmp,
                        hiddenName: 'provinsi_id',
                        valueField: 'provinsi_id',
                        displayField: 'nama_provinsi',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        editable: false,
//                        typeAhead: true,
                        ref: '../kota',
                        triggerAction: 'all',
                        lastQuery: '',
//                        lazyRender: true,
                        mode: 'local',
//                        forceSelection: true,
                        fieldLabel: 'City',
                        store: jun.rztKotaCmp,
                        hiddenName: 'kota_id',
                        valueField: 'kota_id',
                        displayField: 'nama_kota',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
//                        typeAhead: true,
                        ref: '../kecamatan',
                        triggerAction: 'all',
                        lastQuery: '',
//                        lazyRender: true,
                        mode: 'local',
//                        forceSelection: true,
                        fieldLabel: 'Sub District',
                        store: jun.rztKecamatanCmp,
                        hiddenName: 'kecamatan_id',
                        valueField: 'kecamatan_id',
                        displayField: 'nama_kecamatan',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Address',
                        hideLabel: false,
                        //hidden:true,
                        name: 'alamat',
                        id: 'alamatid',
                        ref: '../alamat',
                        enableKeyEvents: true,
                        style : {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        anchor: '100%'
                        //allowBlank:
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Phone',
                        hideLabel: false,
                        //hidden:true,
                        name: 'telp',
                        id: 'telpid',
                        ref: '../telp',
                        maxLength: 25,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Occupation',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kerja',
                        ref: '../kerja',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Origin Branch',
                        ref: '../storeCode',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        value: STORE,
                        readOnly: true,
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.CustomersWin.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.negara.on('select', this.onProvinsiclick, this);
        this.provinsi.on('select', this.onKotaclick, this);
        this.kota.on('select', this.onKecamatanclick, this);
        this.tgl_lahir.on('select', this.onTgllahir, this);
        if (this.modez == 1) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        } else if (this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        }
    },
    onActivate: function(){
        this.onTgllahir();
    },
    onTgllahir: function(){
        var today = new Date();
        var lahir = this.tgl_lahir.getValue();
        if(lahir == undefined || lahir == ""){return;}
        this.age.setValue(today.getFullYear()-lahir.getFullYear());
        //return;
        //var dari = lahir.format("Y-m-d");
        //var sampai = today.format("Y-m-d");
        //Ext.Ajax.request({
        //    url: 'Site/DateDiff',
        //    method: 'POST',
        //    scope: this,
        //    params: {
        //        dari: lahir,
        //        sampai: sampai
        //    },
        //    success: function (f, a) {
        //        var response = Ext.decode(f.responseText);
        //        this.age.setValue(response.msg);
        //    },
        //    failure: function (f, a) {
        //        switch (a.failureType) {
        //            case Ext.form.Action.CLIENT_INVALID:
        //                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
        //                break;
        //            case Ext.form.Action.CONNECT_FAILURE:
        //                Ext.Msg.alert('Failure', 'Ajax communication failed');
        //                break;
        //            case Ext.form.Action.SERVER_INVALID:
        //                Ext.Msg.alert('Failure', a.result.msg);
        //        }
        //    }
        //});
    },
    onProvinsiclick: function () {
        var negara_id = this.negara.getValue();
        this.provinsi.reset();
        this.provinsi.store.clearFilter();
        if (negara_id != '' || negara_id != undefined) {
            this.provinsi.store.filter('negara_id', negara_id, false, true);
        }
    },
    onKotaclick: function () {
        var provinsi_id = this.provinsi.getValue();
        this.kota.reset();
        this.kota.store.clearFilter();
        if (provinsi_id != '' || provinsi_id != undefined) {
            this.kota.store.filter('provinsi_id', provinsi_id, false, true);
        }
    },
    onKecamatanclick: function () {
        var kota_id = this.kota.getValue();
        this.kecamatan.reset();
        this.kecamatan.store.clearFilter();
        if (kota_id != '' || kota_id != undefined) {
            this.kecamatan.store.filter('kota_id', kota_id, false, true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Customers/update/id/' + this.id;
        } else {
            urlz = 'Customers/create/';
        }
        Ext.getCmp('form-Customers').getForm().submit({
            url: urlz,
            scope: this,
            success: function (f, a) {
                jun.rztCustomers.reload();
                jun.rztCustomersCmp.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Customers').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});

//WOMEN
//appendEPCL('+RIB');
//appendEPCL('+C 4');
//appendEPCL('F');
//appendEPCL('T 50 400 0 1 0 75 1 SUB01 777777');
//appendEPCL('T 50 470 0 1 0 65 1 STEPHANUS NOVE ANANDO');
//appendEPCL('T 170 580 0 1 0 45 1 09 2014');
//appendEPCL('T 650 580 0 1 0 45 1 09 2016');
//appendEPCL('I');

//MEN
//appendEPCL('+RIB');
//appendEPCL('+C 4');
//appendEPCL('F');
//appendEPCL('T 50 450 0 1 0 75 1 SUB01 777777');
//appendEPCL('T 50 520 0 1 0 65 1 STEPHANUS NOVE ANANDO');
//appendEPCL('T 535 575 0 1 0 45 1 09 2014');
//appendEPCL('T 785 575 0 1 0 45 1 09 2016');
//appendEPCL('I');

//girls sampe 19 tahun
//appendEPCL('+RIB');
//appendEPCL('+C 4');
//appendEPCL('F');
//appendEPCL('T 50 450 0 1 0 75 1 SUB01 777777');
//appendEPCL('T 50 520 0 1 0 65 1 STEPHANUS NOVE ANANDO');
//appendEPCL('T 170 575 0 1 0 45 1 09 2016');
//appendEPCL('T 785 575 0 1 0 45 1 09 2014');
//appendEPCL('I');
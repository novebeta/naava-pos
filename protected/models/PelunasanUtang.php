<?php

Yii::import('application.models._base.BasePelunasanUtang');

class PelunasanUtang extends BasePelunasanUtang
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->isNewRecord && $this->pelunasan_utang_id == null) {
            $this->pelunasan_utang_id = U::generate_primary_key(RPELUNASANUTANG);
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
}
jun.Barangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Barangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BarangStoreId',
            url: 'Barang',
            autoLoad: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'barang_id'},
                {name: 'kode_barang'},
                {name: 'nama_barang'},
                {name: 'ket'},
                {name: 'grup_id'},
                {name: 'active'},
                {name: 'sat'}
            ]
        }, cfg));
    }
});
jun.rztBarang = new jun.Barangstore();
jun.rztBarangLib = new jun.Barangstore();
jun.rztBarangCmp = new jun.Barangstore({baseParams: {f: "cmp"}});
jun.rztBarangJasa = new jun.Barangstore(
    {
        baseParams: {mode: "jasa", f: "cmp"},
        method: 'POST'
    });
jun.rztBarangNonJasa = new jun.Barangstore(
    {
        baseParams: {mode: "nonjasa", f: "cmp"},
        method: 'POST'
    });
//jun.rztBarang.load();

jun.SysTypesstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SysTypesstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SysTypesStoreId',
            url: 'SysTypes',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sys_types_id'},
                {name: 'type_id'},
                {name: 'type_no'},
                {name: 'next_reference'},
                {name: 'ket'}
            ]
        }, cfg));
    }
});
jun.rztSysTypes = new jun.SysTypesstore();
//jun.rztSysTypes.load();

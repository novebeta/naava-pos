<?php

Yii::import('application.models._base.BaseTenderDetails');
class TenderDetails extends BaseTenderDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function getEmptyDetails()
    {
        $arr_filt = explode(',', SysPrefs::get_val('filter_payment'));
        $new_arr = array();
        foreach($arr_filt as $bank){
            $new_arr[] = "'$bank'";
        }
        $bank_id = implode(',',$new_arr);
        $comm = Yii::app()->db->createCommand("
        SELECT b.bank_id,0 amount FROM nscc_bank b
        WHERE b.bank_id NOT IN ($bank_id) AND store = :store
        ");
        return $comm->queryAll(true,array(
            ':store' => STOREID
        ));
    }
    public function get_added()
    {
        return $this->bank->get_cash_in($this->tender->tgl, $this->tender->store);
    }
    public function get_removed()
    {
        return $this->bank->get_cash_out($this->tender->tgl, $this->tender->store);
    }
    public function get_collected_by_bank()
    {
        $sales = $this->bank->get_total_sales_payment($this->tender->tgl, $this->tender->store);
        $returnsales = $this->bank->get_total_returnsales_payment_bank($this->tender->tgl, $this->tender->store);
        $purchase = 0;
        if ($this->bank->is_bank_cash($this->tender->store)) {
            $purchase = TransferItem::get_total_all_cash($this->tender->tgl, $this->tender->store);
        }
        return $sales + $returnsales - $purchase;
    }
    public function get_collected()
    {
        $sales = Salestrans::get_total_sales($this->tender->tgl, $this->tender->store);
        $purchase = TransferItem::get_total_all_cash($this->tender->tgl, $this->tender->store);
        return $sales - $purchase;
    }
}
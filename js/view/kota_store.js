jun.Kotastore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Kotastore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KotaStoreId',
            url: 'Kota',
            autoLoad: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'kota_id'},
                {name: 'nama_kota'},
                {name: 'provinsi_id'}
            ]
        }, cfg));
    },
    FilterData: function() {
        this.filter([ {
            property: "provinsi_id",
            value: 11
        } ]);
    }

});
jun.rztKota = new jun.Kotastore();
jun.rztKotaLib = new jun.Kotastore();
jun.rztKotaCmp = new jun.Kotastore();
//jun.rztKota.load();

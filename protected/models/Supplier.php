<?php

Yii::import('application.models._base.BaseSupplier');
class Supplier extends BaseSupplier
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function total_hutang_before($from,$store = null)
    {
        $where = "WHERE ";
        $param = array(':from' => $from);
        if ($store != null) {
            $where = "WHERE nti.store = :store AND ";
            $param[':store'] = $store;
            $param[':supplier_id'] = $this->supplier_id;
        }
        $res = app()->db->createCommand("SELECT SUM(c.total) FROM (
(SELECT SUM(nti.total) total
	FROM nscc_transfer_item AS nti $where nti.supplier_id = :supplier_id AND nti.tgl < :from)
UNION
(SELECT SUM(-nti1.total) FROM nscc_transfer_item AS nti
				INNER JOIN nscc_transfer_item AS nti1 ON nti.doc_ref = nti1.doc_ref_other AND nti1.type_ = 1
        $where nti.supplier_id = :supplier_id AND nti1.tgl >= :from)
UNION
(SELECT -SUM(npud.kas_dibayar) total
FROM nscc_transfer_item AS nti
  INNER JOIN nscc_pelunasan_utang_detil AS npud
			ON nti.transfer_item_id = npud.transfer_item_id
  INNER JOIN nscc_pelunasan_utang AS npu
			ON npud.pelunasan_utang_id = npu.pelunasan_utang_id $where nti.supplier_id = :supplier_id
			 AND npu.tgl < :from)) c");
        return $res->queryScalar($param);
    }
    public function get_pelunasan($from,$to,$store){
        $where = "WHERE ";
        $param = array(':from' => $from,':to'=>$to);
        if ($store != null) {
            $where = "WHERE nti.store = :store AND ";
            $param[':store'] = $store;
            $param[':supplier_id'] = $this->supplier_id;
        }
        $res = app()->db->createCommand("(SELECT nti.tgl,nti.doc_ref,'DEBT' note,nti.doc_ref_other no_faktur,
        nti.total hutang,0 payment,nti.total FROM nscc_transfer_item AS nti $where
        nti.supplier_id = :supplier_id AND nti.tgl >= :from AND nti.tgl <= :to)
        UNION
        (SELECT nti1.tgl,nti1.doc_ref,'PAYEMNT' note,nti.doc_ref_other no_faktur,
        0 hutang,-nti.total payment,nti.total FROM nscc_transfer_item AS nti
				INNER JOIN nscc_transfer_item AS nti1 ON nti.doc_ref = nti1.doc_ref_other AND nti1.type_ = 1
        $where nti.supplier_id = :supplier_id AND nti1.tgl >= :from AND nti1.tgl <= :to)
        UNION
        (SELECT npu.tgl,npu.doc_ref,'PAYEMNT' note,nti.doc_ref_other no_faktur,0 hutang,npud.kas_dibayar payment,
        -npud.kas_dibayar total FROM nscc_transfer_item AS nti
        INNER JOIN nscc_pelunasan_utang_detil AS npud
			ON nti.transfer_item_id = npud.transfer_item_id
        INNER JOIN nscc_pelunasan_utang AS npu
			ON npud.pelunasan_utang_id = npu.pelunasan_utang_id
        $where nti.supplier_id = :supplier_id AND npu.tgl >= :from AND npu.tgl <= :to)
        ORDER BY tgl");
        return $res->queryAll(true,$param);
    }
}
<?php

Yii::import('application.models._base.BaseTransferItem');
class TransferItem extends BaseTransferItem
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->isNewRecord && $this->transfer_item_id == null) {
            $this->transfer_item_id = U::generate_primary_key(RTRANSFERITEM);
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public static function get_total_purchase($tgl, $store = STOREID)
    {
        $where = "";
        $param = array(':tgl' => $tgl);
        if ($store != null) {
            $where = "AND store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
    IFNULL(SUM(ns.total), 0) total FROM nscc_transfer_item ns
    WHERE ns.bruto >= 0 AND ns.supplier_id IS NULL AND DATE(ns.tgl) = :tgl $where");
        return $comm->queryScalar($param);
    }
    public static function get_total_returnpurchase($tgl, $store = STOREID)
    {
        $where = "";
        $param = array(':tgl' => $tgl);
        if ($store != null) {
            $where = "AND store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
    IFNULL(SUM(ns.total), 0) total FROM nscc_transfer_item ns
    WHERE ns.bruto < 0 AND ns.supplier_id IS NULL AND DATE(ns.tgl) = :tgl $where");
        return $comm->queryScalar($param);
    }
    public static function get_total_all_cash($tgl, $store = STOREID)
    {
        $where = "";
        $param = array(':tgl' => $tgl);
        if ($store != null) {
            $where = "AND store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
    IFNULL(SUM(ns.total), 0) total FROM nscc_transfer_item ns
    WHERE ns.supplier_id IS NULL AND DATE(ns.tgl) = :tgl $where");
        return $comm->queryScalar($param);
    }

    public static function get_total_tax($tgl, $store = STOREID)
    {
        $where = "";
        $param = array(':tgl' => $tgl);
        if ($store != null) {
            $where = "AND store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
    IFNULL(SUM(ns.vat), 0) total FROM nscc_transfer_item ns
    WHERE DATE(ns.tgl) = :tgl $where");
        return $comm->queryScalar($param);
    }
}
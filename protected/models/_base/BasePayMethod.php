<?php

/**
 * This is the model base class for the table "{{pay_method}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PayMethod".
 *
 * Columns in table "{{pay_method}}" available as properties of the model,
 * followed by relations of table "{{pay_method}}" available as properties of the model.
 *
 * @property integer $pay_method_id
 * @property string $pay_name
 * @property string $ket
 *
 * @property Salestrans[] $salestrans
 */
abstract class BasePayMethod extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{pay_method}}';
	}

	public static function representingColumn() {
		return 'pay_name';
	}

	public function rules() {
		return array(
			array('pay_name', 'required'),
			array('pay_name', 'length', 'max'=>100),
			array('ket', 'length', 'max'=>255),
			array('ket', 'default', 'setOnEmpty' => true, 'value' => null),
			array('pay_method_id, pay_name, ket', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'salestrans' => array(self::HAS_MANY, 'Salestrans', 'pay_method_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'pay_method_id' => Yii::t('app', 'Pay Method'),
			'pay_name' => Yii::t('app', 'Pay Name'),
			'ket' => Yii::t('app', 'Ket'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('pay_method_id', $this->pay_method_id);
		$criteria->compare('pay_name', $this->pay_name, true);
		$criteria->compare('ket', $this->ket, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}
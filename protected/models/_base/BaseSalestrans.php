<?php

/**
 * This is the model base class for the table "{{salestrans}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Salestrans".
 *
 * Columns in table "{{salestrans}}" available as properties of the model,
 * followed by relations of table "{{salestrans}}" available as properties of the model.
 *
 * @property string $salestrans_id
 * @property string $tgl
 * @property string $doc_ref
 * @property string $tdate
 * @property string $user_id
 * @property double $bruto
 * @property double $disc
 * @property double $discrp
 * @property double $totalpot
 * @property double $total
 * @property string $ketdisc
 * @property double $vat
 * @property string $customer_id
 * @property string $doc_ref_sales
 * @property integer $audit
 * @property string $store
 * @property integer $printed
 * @property string $override
 * @property double $bayar
 * @property double $kembali
 * @property string $dokter_id
 * @property integer $log
 * @property integer $up
 * @property double $total_discrp1
 * @property integer $type_
 *
 * @property Payment[] $payments
 * @property Customers $customer
 * @property SalestransDetails[] $salestransDetails
 */
abstract class BaseSalestrans extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{salestrans}}';
	}

	public static function representingColumn() {
		return 'tgl';
	}

	public function rules() {
		return array(
			array('salestrans_id, tgl, doc_ref, tdate, user_id, customer_id, store', 'required'),
			array('audit, printed, log, up, type_', 'numerical', 'integerOnly'=>true),
			array('bruto, disc, discrp, totalpot, total, vat, bayar, kembali, total_discrp1', 'numerical'),
			array('salestrans_id, doc_ref, user_id, doc_ref_sales, override, dokter_id', 'length', 'max'=>50),
			array('ketdisc', 'length', 'max'=>255),
			array('customer_id, store', 'length', 'max'=>20),
			array('bruto, disc, discrp, totalpot, total, ketdisc, vat, doc_ref_sales, audit, printed, override, bayar, kembali, dokter_id, log, up, total_discrp1, type_', 'default', 'setOnEmpty' => true, 'value' => null),
			array('salestrans_id, tgl, doc_ref, tdate, user_id, bruto, disc, discrp, totalpot, total, ketdisc, vat, customer_id, doc_ref_sales, audit, store, printed, override, bayar, kembali, dokter_id, log, up, total_discrp1, type_', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'payments' => array(self::HAS_MANY, 'Payment', 'salestrans_id'),
			'customer' => array(self::BELONGS_TO, 'Customers', 'customer_id'),
			'salestransDetails' => array(self::HAS_MANY, 'SalestransDetails', 'salestrans_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'salestrans_id' => Yii::t('app', 'Salestrans'),
			'tgl' => Yii::t('app', 'Tgl'),
			'doc_ref' => Yii::t('app', 'Doc Ref'),
			'tdate' => Yii::t('app', 'Tdate'),
			'user_id' => Yii::t('app', 'User'),
			'bruto' => Yii::t('app', 'Bruto'),
			'disc' => Yii::t('app', 'Disc'),
			'discrp' => Yii::t('app', 'Discrp'),
			'totalpot' => Yii::t('app', 'Totalpot'),
			'total' => Yii::t('app', 'Total'),
			'ketdisc' => Yii::t('app', 'Ketdisc'),
			'vat' => Yii::t('app', 'Vat'),
			'customer_id' => Yii::t('app', 'Customer'),
			'doc_ref_sales' => Yii::t('app', 'Doc Ref Sales'),
			'audit' => Yii::t('app', 'Audit'),
			'store' => Yii::t('app', 'Store'),
			'printed' => Yii::t('app', 'Printed'),
			'override' => Yii::t('app', 'Override'),
			'bayar' => Yii::t('app', 'Bayar'),
			'kembali' => Yii::t('app', 'Kembali'),
			'dokter_id' => Yii::t('app', 'Dokter'),
			'log' => Yii::t('app', 'Log'),
			'up' => Yii::t('app', 'Up'),
			'total_discrp1' => Yii::t('app', 'Total Discrp1'),
			'type_' => Yii::t('app', 'Type'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('salestrans_id', $this->salestrans_id, true);
		$criteria->compare('tgl', $this->tgl, true);
		$criteria->compare('doc_ref', $this->doc_ref, true);
		$criteria->compare('tdate', $this->tdate, true);
		$criteria->compare('user_id', $this->user_id, true);
		$criteria->compare('bruto', $this->bruto);
		$criteria->compare('disc', $this->disc);
		$criteria->compare('discrp', $this->discrp);
		$criteria->compare('totalpot', $this->totalpot);
		$criteria->compare('total', $this->total);
		$criteria->compare('ketdisc', $this->ketdisc, true);
		$criteria->compare('vat', $this->vat);
		$criteria->compare('customer_id', $this->customer_id);
		$criteria->compare('doc_ref_sales', $this->doc_ref_sales, true);
		$criteria->compare('audit', $this->audit);
		$criteria->compare('store', $this->store, true);
		$criteria->compare('printed', $this->printed);
		$criteria->compare('override', $this->override, true);
		$criteria->compare('bayar', $this->bayar);
		$criteria->compare('kembali', $this->kembali);
		$criteria->compare('dokter_id', $this->dokter_id, true);
		$criteria->compare('log', $this->log);
		$criteria->compare('up', $this->up);
		$criteria->compare('total_discrp1', $this->total_discrp1);
		$criteria->compare('type_', $this->type_);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}
<html>
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="<?php echo bu(); ?>/images/icon-natasha.gif"/>
    <title><?php echo CHtml::encode(Yii::app()->name); ?></title>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/xtheme-natasha2.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/grid/grid-examples.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/css/RowEditor.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/extjs.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/shared/icons/silk.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/aspnet/aspnet.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/silk_v013/silk013.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/form/combos.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/fileuploadfield/css/fileuploadfield.css"/>
    <style>
        #drop {
            border: 2px dashed #BBBBBB;
            border-radius: 5px;
            color: #BBBBBB;
            font: 20pt bold, "Vollkorn";
            padding: 25px;
            text-align: center;
        }

        * {
            font-size: 12px;
            font-family: Candara;
        }
    </style>
</head>
<body>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
<script>
    BASE_URL = '<?=bu()===""?"/":bu();?>';
    SYSTEM_TITLE = '<?= app()->params['system_title']; ?>';
    SYSTEM_SUBTITLE = '<?= app()->params['system_subtitle']; ?>';
    SYSTEM_LOGO = '<img src="<?=bu(); ?>/images/logo.png" alt=""/>';
    SALES_TYPE = '<?=Users::is_audit();?>';
    HEADOFFICE = <?if(defined('HEADOFFICE')){    echo HEADOFFICE ? 'true' : 'false';    }else{    echo 'false';    }?>;
    SYSTEM_BANK_CASH = '<?=Bank::get_bank_cash_id();?>';
    SYSTEM_BANK_CASH_WHILE = '<?=Bank::get_bank_cash_while_id();?>';
    COM_POSIFLEX = localStorage.getItem("serial_com");
    PORT_CLOSED = true;
    Ext.chart.Chart.CHART_URL = '<?=bu(); ?>/js/ext340/resources/charts.swf';
    VALID_CARD = <?=VALID_CARD;?>;
    INTERVALSYNC = <?=INTERVALSYNC;?>;
    ENABLESYNC = <?=ENABLESYNC? 'true' : 'false';?>;
    STORE = '<?=STOREID;?>';
    PRINTER_RECEIPT = '<?=PRINTER_RECEIPT;?>';
    PRINTER_CARD = '<?=PRINTER_CARD;?>';
    SALES_OVERRIDE = '<?
    $id = Yii::app()->user->getId();
    $user = Users::model()->findByPk($id);
    echo Users::get_override($user->user_id,$user->password) ? 1 : 0;
    ?>';
    //    function WebSocketSend(msg) {
    //        if ("WebSocket" in window) {
    //            var ws = new WebSocket("ws://127.0.0.1:1313");
    //            ws.onopen = function () {
    //                if (ws.readyState == 1) {
    //                    ws.send(msg);
    //                    ws.close();
    //                }
    //            };
    //        }
    //        else {
    //            alert("WebSocket NOT supported by your Browser!");
    //        }
    //    }
    //    function menuDetil(kdbrg, subtotal, labelTotal, total) {
    //        WebSocketSend("menuDetil;" + kdbrg + ";" + subtotal + ";" + labelTotal + ";" + total);
    //    }
    //    function displayTextAt(col, row, msg, mode) {
    //        WebSocketSend("displayTextAt;" + col + ";" + row + ";" + mode + ";" + msg);
    //    }
    //    function setBlinkRate(rate) {
    //        WebSocketSend("setBlinkRate;" + rate);
    //    }
    //    function setCursorRow(rate) {
    //        WebSocketSend("setCursorRow;" + rate);
    //    }
    //    function setCursorColumn(rate) {
    //        WebSocketSend("setCursorColumn;" + rate);
    //    }
</script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/TableGrid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/GroupSummary.js"></script>
<?
$dir = array('/js/view/');
foreach ($dir as $path) {
    $templatePath = dirname(Yii::app()->basePath) . $path;
    $files = scandir($templatePath);
    foreach ($files as $file) {
        if (is_file($templatePath . '/' . $file)) {
            ?>
            <script type="text/javascript" src="<?php echo(bu() . $path . $file); ?>"></script>
        <?
        }
    }
}
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/mainpanel.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
<?php echo $content; ?>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/sha512.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/fileuploadfield/FileUploadField.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/cc.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/deployJava.js"></script>
<script type="text/javascript">
/**
 * Optionally used to deploy multiple versions of the applet for mixed
 * environments.  Oracle uses document.write(), which puts the applet at the
 * top of the page, bumping all HTML content down.
 */
function is_enable_tools() {
    var enable_tools = localStorage.getItem("enable_tools");
    if (enable_tools == "" || enable_tools == undefined) {
        return false;
    }
    return enable_tools != "false";
}
if (is_enable_tools()) {
    deployQZ();
}
/**
 * Deploys different versions of the applet depending on Java version.
 * Useful for removing warning dialogs for Java 6.  This function is optional
 * however, if used, should replace the <applet> method.  Needed to address
 * MANIFEST.MF TrustedLibrary=true discrepency between JRE6 and JRE7.
 */
function deployQZ() {
    var attributes = {
        id: "qz", code: 'qz.PrintApplet.class',
        archive: 'qz-print.jar', width: 1, height: 1
    };
    var parameters = {
        jnlp_href: 'qz-print_jnlp.jnlp',
        cache_option: 'plugin', disable_logging: 'false',
        initial_focus: 'false'
    };
    if (deployJava.versionCheck("1.7+") == true) {
    }
    else if (deployJava.versionCheck("1.6+") == true) {
        attributes['archive'] = 'jre6/qz-print.jar';
        parameters['jnlp_href'] = 'jre6/qz-print_jnlp.jnlp';
    }
    deployJava.runApplet(attributes, parameters, '1.5');
}
function qzReady() {
    // Setup our global qz object
    window["qz"] = document.getElementById('qz');
    if (qz) {
        try {
            console.log('Using qz version ' + qz.getVersion());
        } catch (err) { // LiveConnect error, display a detailed meesage
            console.log("ERROR:  \nThe applet did not load correctly.  Communication to the " +
            "applet has failed, likely caused by Java Security Settings.  \n\n" +
            "CAUSE:  \nJava 7 update 25 and higher block LiveConnect calls " +
            "once Oracle has marked that version as outdated, which " +
            "is likely the cause.  \n\nSOLUTION:  \n  1. Update Java to the latest " +
            "Java version \n          (or)\n  2. Lower the security " +
            "settings from the Java Control Panel.");
        }
    }
}
function notReady() {
    // If applet is not loaded, display an error

    if (!isLoaded()) {
        return true;
    }
    // If a printer hasn't been selected, display a message.
    else if (!qz.getPrinter()) {
        console.log('Please select a printer first by using the "Detect Printer" button.');
        return true;
    }
    return false;
}
/**
 * Returns is the applet is not loaded properly
 */
function isLoaded() {
    if (!is_enable_tools()) {
        return false;
    }
    if (!qz) {
        console.log('Error:\n\n\tPrint plugin is NOT loaded!');
        return false;
    } else {
        try {
            if (!qz.isActive()) {
                console.log('Error:\n\n\tPrint plugin is loaded but NOT active!');
                return false;
            }
        } catch (err) {
            console.log('Error:\n\n\tPrint plugin is NOT loaded properly!');
            return false;
        }
    }
    return true;
}
//if (isLoaded()) {
//    qz.findPrinter();
//    window['qzDoneFinding'] = function () {
//        var printer = qz.getPrinter();
//        console.log(printer !== null ? 'Default printer found: "' + printer + '"' :
//        'Default printer ' + 'not found');
//        window['qzDoneFinding'] = null;
//    };
//}
function findPrinterReceipt() {
    var name = PRINTER_RECEIPT;
    if (isLoaded()) {
        qz.findPrinter(name);
        window['qzDoneFinding'] = function () {
            var printer = qz.getPrinter();
            console.log(printer !== null ? 'Printer found: "' + printer +
            '" after searching for "' + name + '"' : 'Printer "' +
            name + '" not found.');
            window['qzDoneFinding'] = null;
        };
        while (!qz.isDoneFinding()) {
        }
    }
}
function findPrinterCard() {
    var name = PRINTER_CARD;
    if (isLoaded()) {
        qz.findPrinter(name);
        window['qzDoneFinding'] = function () {
            var printer = qz.getPrinter();
            console.log(printer !== null ? 'Printer found: "' + printer +
            '" after searching for "' + name + '"' : 'Printer "' +
            name + '" not found.');
            window['qzDoneFinding'] = null;
        };
        while (!qz.isDoneFinding()) {
        }
    }
}
function findDefaultPrinter() {
    if (isLoaded()) {
        qz.findPrinter();
        window['qzDoneFinding'] = function () {
            var printer = qz.getPrinter();
            console.log(printer !== null ? 'Printer found: "' + printer +
            '" after searching for "' + name + '"' : 'Printer "' +
            name + '" not found.');
            window['qzDoneFinding'] = null;
        };
        while (!qz.isDoneFinding()) {
        }
    }
}
function qzDonePrinting() {
    // Alert error, if any
    if (qz.getException()) {
        console.log('Error printing:\n\n\t' + qz.getException().getLocalizedMessage());
        qz.clearException();
        return;
    }
    console.log('Successfully sent print data to "' + qz.getPrinter() + '" queue.');
}
function appendEPCL(data) {
    if (data == null || data.length == 0) {
        return;
    }
    qz.appendHex('x1b');
    qz.append(data);
    qz.appendHex('x0D');
}
function fixHTML(html) {
    return html.replace(/ /g, "&nbsp;").replace(/’/g, "'").replace(/-/g, "&#8209;");
}
function getPath() {
    var path = window.location.href;
    return path.substring(0, path.lastIndexOf("/")) + "/";
}
function chr(i) {
    return String.fromCharCode(i);
}
function sendSerialData(msg) {
    if (isLoaded()) {
        qz.openPort(COM_POSIFLEX);
        window['qzDoneOpeningPort'] = function (portName) {
            if (qz.getException()) {
                console.log("Could not open port [" + portName + "] \n\t" +
                qz.getException().getLocalizedMessage());
                qz.clearException();
            } else {
                PORT_CLOSED = false;
                qz.setSerialBegin(chr(2));
                qz.setSerialEnd(chr(13));
                qz.setSerialProperties("9600", "8", "1", "none", "none");
                qz.send(COM_POSIFLEX, '\x1B\x40');
                qz.send(COM_POSIFLEX, msg);
                qz.sendHex(COM_POSIFLEX, 'x1fx43x00');
                window['qzSerialReturned'] = function (portName, data) {
                    qz.closePort(COM_POSIFLEX);
                    // Automatically called when "qz.closePort() is finished (even if it fails to close)
                    window['qzDoneClosingPort'] = function (portName) {
                        PORT_CLOSED = true;
                        if (qz.getException()) {
                            console.log("Could not close port [" + portName + "] \n\t" +
                            qz.getException().getLocalizedMessage());
                            qz.clearException();
                        } else {
                            console.log("Port [" + portName + "] closed!");
                        }
                    };
                };
            }
        };
    }
}
function sendSerialDataHex(msg) {
    if (isLoaded()) {
        qz.openPort(COM_POSIFLEX);
        window['qzDoneOpeningPort'] = function (portName) {
            if (qz.getException()) {
                console.log("Could not open port [" + portName + "] \n\t" +
                qz.getException().getLocalizedMessage());
                qz.clearException();
            } else {
                PORT_CLOSED = false;
                qz.setSerialBegin(chr(2));
                qz.setSerialEnd(chr(13));
                qz.setSerialProperties("9600", "8", "1", "none", "none");
                qz.sendHex(COM_POSIFLEX, msg);
                window['qzSerialReturned'] = function (portName, data) {
                    qz.closePort(COM_POSIFLEX);
                    // Automatically called when "qz.closePort() is finished (even if it fails to close)
                    window['qzDoneClosingPort'] = function (portName) {
                        PORT_CLOSED = true;
                        if (qz.getException()) {
                            console.log("Could not close port [" + portName + "] \n\t" +
                            qz.getException().getLocalizedMessage());
                            qz.clearException();
                        } else {
                            console.log("Port [" + portName + "] closed!");
                        }
                    };
                };
            }
        };
    }
}
function clearText() {
    sendSerialData('\x1B\x3D\x02\x0C\x1F\x01');
}
function displayText(msg, mode) {
    if (msg.length > 40) {
        msg = msg.substr(0, 39);
    }
    sendSerialData(msg);
}
function twoRows(msg1, msg2) {
    var lmsg1 = msg1.length;
    var lmsg2 = msg2.length;
    if (lmsg1.length > 20) {
        msg1 = msg1.substring(0, 19);
        lmsg1 = 20;
    }
    for (i = lmsg1; i < 20; i++) {
        msg1 += " ";
    }
    if (lmsg2 > 20) {
        msg2 = msg2.substring(0, 19);
        lmsg2 = 20;
    }
    sendSerialData(msg1 + msg2);
}
function menuDetil(brg, subtotal, labelTotal, total) {
    subtotal = subtotal.toString();
    total = total.toString();
    var lbrg = brg.length;
    var lsubtotal = subtotal.length;
    var llabeltotal = labelTotal.length;
    var ltotal = total.length;
    if (lbrg > 10) {
        brg = brg.substring(0, 9);
        lbrg = 10;
    }
    var totalrow1 = lbrg + lsubtotal;
    for (var i = totalrow1; i < 20; i++) {
        brg += " ";
    }
    if (llabeltotal > 10) {
        labelTotal = labelTotal.substring(0, 9);
        llabeltotal = 10;
    }
    var totalrow2 = llabeltotal + ltotal;
    for (var i = totalrow2; i < 20; i++) {
        labelTotal += " ";
    }
    var row1 = brg + subtotal;
    var row2 = labelTotal + total;
    sendSerialData(row1 + row2);
}
</script>
</body>
</html>

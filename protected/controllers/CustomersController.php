<?php
class CustomersController extends GxController
{
    public function actionCreate()
    {
        $model = new Customers;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $ref = new Reference();
                $docref = $ref->get_next_reference(CUSTOMER);
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Customers'][$k] = $v;
                }
                $_POST['Customers']['customer_id'] = $docref;
                $_POST['Customers']['no_customer'] = Customers::generete_no_customer(STOREID);
                $_POST['Customers']['awal'] = new CDbExpression('NOW()');
                $_POST['Customers']['akhir'] = new CDbExpression('NOW()');
                $model->attributes = $_POST['Customers'];
                $msg = t('save.fail', 'app');
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales')) . CHtml::errorSummary($model));
                }
                $ref->save(CUSTOMER, $model->customer_id, $docref);
                $transaction->commit();
                $msg = t('save.success', 'app') . "<br>Customer No : " . $model->no_customer;
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Customers');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Customers'][$k] = $v;
            }
            $msg = t('save.fail', 'app');
            $model->attributes = $_POST['Customers'];
            if ($model->save()) {
                $status = true;
                $msg = t('save.success.id', 'app', array('{id}' => $model->customer_id));
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->customer_id));
            }
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $order = "nc.awal";
        $query = Yii::app()->db->createCommand();
        $query->select("nc.customer_id,nc.nama_customer,nc.no_customer,nc.tempat_lahir,nc.tgl_lahir,nc.email,
            nc.telp,nc.alamat,nc.awal,nc.akhir,nc.store,nc.kecamatan_id,nc.kota_id,nc.provinsi_id,
            nc.negara_id,nc.status_cust_id,nc.sex,nc.kerja");
        $query->from = '{{customers}} AS nc';
//        $query->leftJoin('{{kecamatan}} AS nk', '(nc.kecamatan_id = nk.kecamatan_id)');
//        $query->leftJoin('{{kota}} AS nko', '(nk.kota_id = nko.kota_id)');
//        $query->leftJoin('{{provinsi}} AS np', '(nko.provinsi_id = np.provinsi_id)');
        $query->order = $order;
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['query'])) {
            $param = array(':nama_customer' => "%" . $_POST['query'] . "%",
                ':alamat' => "%" . $_POST['query'] . "%",
                ':no_customer' => "%" . $_POST['query'] . "%",
                ':telp' => "%" . $_POST['query'] . "%");
            $query->andWhere('nama_customer like :nama_customer OR alamat LIKE :alamat
            OR no_customer LIKE :no_customer OR telp LIKE :telp', $param
            );
            $query->limit($limit, $start);
        }
        if (isset($_POST['no_customer'])) {
//            $criteria->addCondition("no_customer like :no_customer");
//            $param[':no_customer'] = "%" . $_POST['no_customer'] . "%";
            $par[':no_customer'] = "%" . $_POST['no_customer'] . "%";
            $query->andWhere('no_customer like :no_customer', $par);
            $param[':no_customer'] = "%" . $_POST['no_customer'] . "%";
        }
        if (isset($_POST['nama_customer'])) {
//            $criteria->addCondition("nama_customer like :nama_customer");
//            $param[':nama_customer'] = "%" . $_POST['nama_customer'] . "%";
            $par[':nama_customer'] = "%" . $_POST['nama_customer'] . "%";
            $query->andWhere('nama_customer like :nama_customer', $par);
            $param[':nama_customer'] = "%" . $_POST['nama_customer'] . "%";
        }
        if (isset($_POST['telp'])) {
//            $criteria->addCondition("tempat_lahir like :tempat_lahir");
//            $param[':tempat_lahir'] = "%" . $_POST['tempat_lahir'] . "%";
            $par[':telp'] = "%" . $_POST['telp'] . "%";
            $query->andWhere('telp like :telp', $par);
            $param[':telp'] = "%" . $_POST['telp'] . "%";
        }
        if (isset($_POST['alamat'])) {
//            $criteria->addCondition("tgl_lahir = :tgl_lahir");
//            $param[':tgl_lahir'] = $_POST['tgl_lahir'];
            $par[':alamat'] = "%" . $_POST['alamat'] . "%";
            $query->andWhere('alamat like :alamat', $par);
            $param[':alamat'] = "%" . $_POST['alamat'] . "%";
        }
        if (isset($_POST['customer_id'])) {
//            $criteria->addCondition("tgl_lahir = :tgl_lahir");
//            $param[':tgl_lahir'] = $_POST['tgl_lahir'];
            $par = array();
            $par[':customer_id'] = $_POST['customer_id'];
            $query->params = array();
            $query->where('customer_id = :customer_id', $par);
            $param = array();
            $param[':customer_id'] = $_POST['customer_id'];
        }
//        $criteria->params = $param;
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $query->limit($limit, $start);
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
        }
//        $model = Customers::model()->findAll($criteria);
        $criteria->addCondition($query->getWhere());
        $criteria->params = $param;
        $total = Customers::model()->count($criteria);
//        $this->renderJson($model, $total);
        $this->renderJsonArrWithTotal($query->queryAll(true, $param), $total);
    }
    public function actionImport()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $stat = StatusCust::model()->find("nama_status = :nama_status", array(':nama_status' => 'REG'));
                $ref = new Reference();
                $docref = $ref->get_next_reference(CUSTOMER);
                $arr_key = array_keys($detils);
                $duplicate_no_cust = array();
                foreach ($detils[$arr_key[0]] as $row) {
                    $arr_key = array_keys($row);
                    $arr_idx = array();
                    foreach ($arr_key as $key) {
                        if (strpos($key, 'NOBASE') !== false) {
                            $arr_idx[0] = $key;
                        }
                        if (strpos($key, 'NAMACUS') !== false) {
                            $arr_idx[1] = $key;
                        }
                        if (strpos($key, 'ALAMAT') !== false) {
                            $arr_idx[2] = $key;
                        }
                        if (strpos($key, 'TELP') !== false) {
                            $arr_idx[3] = $key;
                        }
                        if (strpos($key, 'TGLLH') !== false) {
                            $arr_idx[4] = $key;
                        }
                        if (strpos($key, 'SEX') !== false) {
                            $arr_idx[5] = $key;
                        }
                        if (strpos($key, 'KERJA') !== false) {
                            $arr_idx[6] = $key;
                        }
                        if (strpos($key, 'AWAL') !== false) {
                            $arr_idx[7] = $key;
                        }
                        if (strpos($key, 'AKHIR') !== false) {
                            $arr_idx[8] = $key;
                        }
                    }
                    $cust = Customers::model()->find('no_customer = :no_customer', array(':no_customer' => $row[$arr_key[1]]));
                    if ($cust != null) {
                        $duplicate_no_cust[] = array($row[$arr_idx[0]], $row[$arr_idx[1]], $row[$arr_idx[2]],
                            $row[$arr_idx[3]], $row[$arr_idx[4]], $row[$arr_idx[5]], $row[$arr_idx[6]], $row[$arr_idx[7]],
                            $row[$arr_idx[8]]);
                        continue;
                    }
                    $model = new Customers;
                    $model->customer_id = $docref;
                    $model->no_customer = $row[$arr_idx[0]];
                    $model->nama_customer = $row[$arr_idx[1]];
                    $model->alamat = $row[$arr_idx[2]];
                    $model->telp = $row[$arr_idx[3]];
                    $tgl_lahir = strtotime($row[$arr_idx[4]]);
                    $model->tgl_lahir = $tgl_lahir ? date('Y-m-d', $tgl_lahir) : null;
                    $model->sex = $row[$arr_idx[5]];
                    $model->kerja = $row[$arr_idx[6]];
                    $model->status_cust_id = $stat->status_cust_id;
                    $awal = strtotime($row[$arr_idx[7]]);
                    $model->awal = $awal ? date('Y-m-d', $awal) : null;
                    $akhir = strtotime($row[$arr_idx[8]]);
                    $model->akhir = $akhir ? date('Y-m-d', $akhir) : null;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Customers')) . CHtml::errorSummary($model));
                    }
                    $docref = $ref->_customers($docref);
                }
//                $msg = t('save.fail', 'app');
                $ref->save(CUSTOMER, $model->customer_id, $docref);
                $transaction->commit();
                $msg = '<table cellspacing="5" style="border:1px solid #BBBBBB;border-collapse:collapse;padding:5px; font-size: 10pt;">
                        <thead>
                            <tr>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;" >No. Customers</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Customer Name</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Address</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Telp</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">BirthDate</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Sex</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Occuption</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">First</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Last</th>
                            </tr>
                        </thead><tbody >';
                foreach ($duplicate_no_cust as $r) {
                    $msg .= "<tr>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[0]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[1]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[2]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[3]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[4]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[5]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[6]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[7]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[8]</td>
                            </tr>";
                }
                $msg .= "</tbody></table>";
//                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionSaveLogCard()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $model = $this->loadModel($_POST['customer_id'], 'Customers');
            if ($model != null) {
                $model->cardtype = $_POST['cardtype'];
                $model->validcard = $_POST['validcard'];
                $model->printcarddate = $_POST['printcarddate'];
                $model->locprintcard = $_POST['locprintcard'];
                $model->save();
                echo CJSON::encode(array(
                    'success' => true,
                    'msg' => 'Saved successfully'
                ));
            } else {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Customer not valid'
                ));
            }
            Yii::app()->end();
        }
    }
    public function actionCreateLog()
    {
        $cust = Customers::get_backup();
        echo CJSON::encode(array(
            'url' => SysPrefs::get_val('url_nars'),
            'username' => SysPrefs::get_val('username_nars'),
            'password' => SysPrefs::get_val('password_nars'),
            'cabang' => STOREID,
            'tanggal' => date("Y-m-d"),
            'jenis' => 'pasien',
            'data' => CJSON::encode($cust)
        ));
    }
}
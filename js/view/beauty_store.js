jun.Beautystore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Beautystore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BeautyStoreId',
            url: 'Beauty',
            autoLoad: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'beauty_id'},
                {name: 'nama_beauty'},
                {name: 'gol_id'},
                {name: 'kode_beauty'},
                {name: 'active'},
                {name: 'store'}
            ]
        }, cfg));
    }
});
jun.rztBeauty = new jun.Beautystore();
jun.rztBeautyLib = new jun.Beautystore({
    baseParams: {mode: "lib"},
    method: 'POST'
});
jun.rztBeautyCmp = new jun.Beautystore({baseParams: {f: "cmp"}});
//jun.rztBeauty.load();

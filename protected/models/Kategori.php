<?php

Yii::import('application.models._base.BaseKategori');
class Kategori extends BaseKategori
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function is_have_stock()
    {
        return $this->have_stock == 1;
    }
}
<?php

Yii::import('application.models._base.BaseGrupAttr');
class GrupAttr extends BaseGrupAttr
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function save_grup_attr($vat, $tax, $coa_jual, $coa_sales_disc, $coa_sales_hpp,
                                     $coa_purchase, $coa_purchase_disc, $coa_purchase_return,
                                     $store, $up, $grup_id)
    {
        $comm = Yii::app()->db->createCommand(
            "REPLACE INTO nscc_grup_attr (vat, tax, coa_jual, coa_sales_disc,
              coa_sales_hpp,coa_purchase,coa_purchase_disc,coa_purchase_return,
              store,up,grup_id)
              VALUES (:vat, :tax, :coa_jual, :coa_sales_disc, :coa_sales_hpp,
              :coa_purchase, :coa_purchase_disc, :coa_purchase_return,
              :store, :up,:grup_id)"
        );
        return $comm->execute(array(
            ':vat' => $vat, ':tax' => $tax,
            ':coa_jual' => $coa_jual, ':coa_sales_disc' => $coa_sales_disc,
            ':coa_sales_hpp' => $coa_sales_hpp, ':coa_purchase' => $coa_purchase,
            ':coa_purchase_disc' => $coa_purchase_disc, ':coa_purchase_return' => $coa_purchase_return,
            ':store' => $store, ':up' => $up, ':grup_id' => $grup_id
        ));
    }
}
<?php

/**
 * This is the model base class for the table "{{salestrans_details}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "SalestransDetails".
 *
 * Columns in table "{{salestrans_details}}" available as properties of the model,
 * followed by relations of table "{{salestrans_details}}" available as properties of the model.
 *
 * @property string $salestrans_details
 * @property integer $barang_id
 * @property string $salestrans_id
 * @property integer $qty
 * @property double $disc
 * @property double $discrp
 * @property string $ketpot
 * @property double $vat
 * @property double $vatrp
 * @property double $bruto
 * @property double $total
 * @property double $total_pot
 * @property double $price
 * @property double $jasa_dokter
 * @property string $dokter_id
 * @property string $disc_name
 * @property double $hpp
 * @property double $cost
 * @property double $disc1
 * @property double $discrp1
 *
 * @property BeautyServices[] $beautyServices
 * @property Salestrans $salestrans
 * @property Dokter $dokter
 * @property Barang $barang
 */
abstract class BaseSalestransDetails extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{salestrans_details}}';
	}

	public static function representingColumn() {
		return 'ketpot';
	}

	public function rules() {
		return array(
			array('salestrans_details, barang_id, salestrans_id', 'required'),
			array('barang_id, qty', 'numerical', 'integerOnly'=>true),
			array('disc, discrp, vat, vatrp, bruto, total, total_pot, price, jasa_dokter, hpp, cost, disc1, discrp1', 'numerical'),
			array('salestrans_details, salestrans_id, dokter_id', 'length', 'max'=>50),
			array('ketpot', 'length', 'max'=>255),
			array('disc_name', 'length', 'max'=>100),
			array('qty, disc, discrp, ketpot, vat, vatrp, bruto, total, total_pot, price, jasa_dokter, dokter_id, disc_name, hpp, cost, disc1, discrp1', 'default', 'setOnEmpty' => true, 'value' => null),
			array('salestrans_details, barang_id, salestrans_id, qty, disc, discrp, ketpot, vat, vatrp, bruto, total, total_pot, price, jasa_dokter, dokter_id, disc_name, hpp, cost, disc1, discrp1', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'beautyServices' => array(self::HAS_MANY, 'BeautyServices', 'salestrans_details'),
			'salestrans' => array(self::BELONGS_TO, 'Salestrans', 'salestrans_id'),
			'dokter' => array(self::BELONGS_TO, 'Dokter', 'dokter_id'),
			'barang' => array(self::BELONGS_TO, 'Barang', 'barang_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'salestrans_details' => Yii::t('app', 'Salestrans Details'),
			'barang_id' => Yii::t('app', 'Barang'),
			'salestrans_id' => Yii::t('app', 'Salestrans'),
			'qty' => Yii::t('app', 'Qty'),
			'disc' => Yii::t('app', 'Disc'),
			'discrp' => Yii::t('app', 'Discrp'),
			'ketpot' => Yii::t('app', 'Ketpot'),
			'vat' => Yii::t('app', 'Vat'),
			'vatrp' => Yii::t('app', 'Vatrp'),
			'bruto' => Yii::t('app', 'Bruto'),
			'total' => Yii::t('app', 'Total'),
			'total_pot' => Yii::t('app', 'Total Pot'),
			'price' => Yii::t('app', 'Price'),
			'jasa_dokter' => Yii::t('app', 'Jasa Dokter'),
			'dokter_id' => Yii::t('app', 'Dokter'),
			'disc_name' => Yii::t('app', 'Disc Name'),
			'hpp' => Yii::t('app', 'Hpp'),
			'cost' => Yii::t('app', 'Cost'),
			'disc1' => Yii::t('app', 'Disc1'),
			'discrp1' => Yii::t('app', 'Discrp1'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('salestrans_details', $this->salestrans_details, true);
		$criteria->compare('barang_id', $this->barang_id);
		$criteria->compare('salestrans_id', $this->salestrans_id);
		$criteria->compare('qty', $this->qty);
		$criteria->compare('disc', $this->disc);
		$criteria->compare('discrp', $this->discrp);
		$criteria->compare('ketpot', $this->ketpot, true);
		$criteria->compare('vat', $this->vat);
		$criteria->compare('vatrp', $this->vatrp);
		$criteria->compare('bruto', $this->bruto);
		$criteria->compare('total', $this->total);
		$criteria->compare('total_pot', $this->total_pot);
		$criteria->compare('price', $this->price);
		$criteria->compare('jasa_dokter', $this->jasa_dokter);
		$criteria->compare('dokter_id', $this->dokter_id);
		$criteria->compare('disc_name', $this->disc_name, true);
		$criteria->compare('hpp', $this->hpp);
		$criteria->compare('cost', $this->cost);
		$criteria->compare('disc1', $this->disc1);
		$criteria->compare('discrp1', $this->discrp1);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}
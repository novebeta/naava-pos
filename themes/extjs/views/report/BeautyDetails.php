<h1>Beauty Details</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle = 'Beauty Details';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => array('doc_ref', 'nama_beauty', 'nama_customer'),
    'extraRowColumns' => array('kode_beauty'),
    'extraRowPos' => 'below',
    'extraRowTotals' => function ($data, $row, &$totals) {
        if (!isset($totals['sum'])) {
            $totals['sum'] = 0;
        }
        $totals['sum'] += $data['beauty_tip'];
    },
    'extraRowExpression' => '"<span class=\"subtotal\">Total ".$data["nama_beauty"]." Services : ".format_number_report($totals["sum"])."</span>"',
    'columns' => array(
        array(
            'header' => 'Date',
            'name' => 'tgl'
        ),
        array(
            'header' => 'Beautician Name',
            'name' => 'nama_beauty'
        ),
        array(
            'header' => 'Customer',
            'name' => 'nama_customer'
        ),
        array(
            'header' => 'No. Natasha Receipt',
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'Item Code',
            'name' => 'kode_barang',
            'footer' => "Total All Services"
        ),
        array(
            'header' => 'Total Services Tips',
            'name' => 'beauty_tip',
            'value' => function ($data) {
                return format_number_report($data['beauty_tip']);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total_beauty_tip)
        )
    ),
));
?>
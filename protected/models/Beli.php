<?php

Yii::import('application.models._base.BaseBeli');
class Beli extends BaseBeli
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function save_default_price($barang_id, $price, $tax, $store = STOREID)
    {
        $comm = Yii::app()->db->createCommand(
            "REPLACE INTO nscc_beli (price, tax, store, up, barang_id)
                VALUES (:price, :tax, :store, 0, :barang_id)"
        );
        return $comm->execute(array(':price' => $price, ':tax' => $tax, ':store' => $store, ':barang_id' => $barang_id));
    }
    public static function save_default_price_by_region($kode_barang, $price, $tax, $wilayah_id)
    {
        $comm = Yii::app()->db->createCommand(
            "UPDATE nscc_beli nb INNER JOIN nscc_store ns
                    ON nb.store = ns.store_kode
                INNER JOIN nscc_barang nb1
                    ON nb.barang_id = nb1.barang_id
                SET nb.price = :price, nb.tax = :tax
            WHERE ns.wilayah_id = :wilayah_id AND nb1.kode_barang = :kode_barang");
        return $comm->execute(array(
            ':wilayah_id' => $wilayah_id,
            ':price' => $price,
            ':tax' => $tax,
            ':kode_barang' => $kode_barang,
        ));
    }
    public function beforeValidate()
    {
        if ($this->isNewRecord && $this->beli_id == null) {
            $this->beli_id = U::generate_primary_key(RBELI);
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        return parent::beforeValidate();
    }
}
<?php

Yii::import('application.models._base.BaseKas');
Yii::import('application.components.U');
class Kas extends BaseKas
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->isNewRecord && $this->kas_id == null) {
            $this->kas_id = U::generate_primary_key(RKAS);
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        if ($this->visible == null) {
            $this->visible = 1;
        }
        return parent::beforeValidate();
    }
    public static function is_modal_exist($tgl)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("total > 0");
        $criteria->addCondition("type_ = 1");
        $criteria->addCondition("DATE(tgl) = :tgl");
        $criteria->params = array(':tgl' => $tgl);
        $res = Kas::model()->count($criteria);
        return $res > 0;
    }
    public static function get_cash_in($tgl, $store = STOREID)
    {
        $where = "";
        $param = array(':tgl' => $tgl);
        if ($store != null) {
            $where = "AND store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
    IFNULL(SUM(ns.total), 0) total FROM nscc_kas ns
    WHERE ns.arus = 1 AND ns.visible = 1 AND DATE(ns.tgl) = :tgl $where");
        return $comm->queryScalar($param);
    }
    public static function get_cash_out($tgl, $store = STOREID)
    {
        $where = "";
        $param = array(':tgl' => $tgl);
        if ($store != null) {
            $where = "AND store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
    IFNULL(SUM(ns.total), 0) total FROM nscc_kas ns
    WHERE ns.arus = -1 AND ns.visible = 1 AND DATE(ns.tgl) = :tgl $where");
        return $comm->queryScalar($param);
    }
//    protected function afterSave()
//    {
//        parent::afterSave();
//        $is_in = $this->total >= 0;
//        U::add_bank_trans($is_in ? CASHIN : CASHOUT, $this->kas_id, $this->bank_id, $this->doc_ref, $this->tgl,
//            $this->total, $this->user_id);
//    }
}